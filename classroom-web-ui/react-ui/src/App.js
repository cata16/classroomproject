import './App.css';
import {connect} from "react-redux";
import { Component } from 'react';
import { Route, Switch } from 'react-router';
import LoginContainer from './component/commons/LoginContainer';
import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/jelly.css';
import {urls} from '../src/utils/utils';
import { Router } from 'react-router-dom';
import {history} from './helpers/history';
import './locales/i18n';
import PrivateRoute from './helpers/privateRoute';
import StudentHomeContainer from './component/student/StudentHomeContainer';
import StudentProfileContainer from './component/student/StudentProfileContainer';
import StudentCourseContainer from './component/student/StudentCourseContainer';
import AssignmentDetailsContainer from './component/student/AssignmentDetailsContainer';
import userEvent from '@testing-library/user-event';
import TeacherHomeContainer from './component/teacher/TeacherHomeContainer';
import TeacherProfileContainer from './component/teacher/TeacherProfileContainer';
import TeacherCourse from './component/teacher/TeacherCourse';
// import 'bootstrap/dist/css/bootstrap.min.css';  
// import 'bootstrap/dist/js/bootstrap.bundle.min.js';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      
    }
  }

  render() {
    return (
        <div style={{height:'100vh'}}>
          <Router className="" history={history}>
            <div style={{height:'100%'}}>
              <Switch> 
                <Route exact path={urls.login} component={LoginContainer}/>
                {this.props.roles.includes("Teacher")? 
                <Switch> 
                  <PrivateRoute path={urls.class + "/:id"} component={TeacherCourse} authed={this.props.isLoggedIn} accessRole={["Teacher"]} userRoles={this.props.roles} />
                  <PrivateRoute path={urls.profileTeacher} component={TeacherProfileContainer} authed={this.props.isLoggedIn} accessRole={["Teacher"]} userRoles={this.props.roles}/>
                  <PrivateRoute path={urls.teacher} component={TeacherHomeContainer} authed={this.props.isLoggedIn} accessRole={["Teacher"]} userRoles={this.props.roles}/>
                </Switch>: null }
                {this.props.roles.includes("Student")? 
                <Switch>
                  {/* <PrivateRoute path={urls.class + "/:id" + urls.assignment} component={AssignmentDetailsContainer} authed={this.props.isLoggedIn}/> */}
                  <PrivateRoute path={urls.classStudent + "/:id"} component={StudentCourseContainer} authed={this.props.isLoggedIn} accessRole={["Student"]} userRoles={this.props.roles}/>
                  <PrivateRoute path={urls.profileStudent} component={StudentProfileContainer} authed={this.props.isLoggedIn} accessRole={["Student"]} userRoles={this.props.roles}/>
                  <PrivateRoute path={urls.student} component={StudentHomeContainer} authed={this.props.isLoggedIn} accessRole={["Student"]} userRoles={this.props.roles}/>
                </Switch>: null }
                <Route exact path={"/"} component={LoginContainer}/>
                
                {/* <PrivateRoute path={urls.profileStudent} component={StudentProfileContainer} authed={this.props.isLoggedIn}/> */}
                
                {/* <PrivateRoute path={urls.assignmentDetails} component={AssignmentDetailsContainer} authed={this.props.isLoggedIn}/> */}
              </Switch>
            </div>
          </Router>
          <Alert stack={{limit: 4}}/>
        </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.auth.isLoggedIn,
    roles: state.auth.roles,
  }
};

export default connect(mapStateToProps, {}) (App);
