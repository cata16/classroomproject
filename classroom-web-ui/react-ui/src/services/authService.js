import auth from '../api/auth.js';

class AuthService {
    login(email, password) {
        let userCredentials = JSON.stringify({
            email: email,
            password: password
        });
    
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        return auth.post("/signin", userCredentials, axiosConfig);
    }

    resetPassword(email) {
        let userCredentials = JSON.stringify({
            email: email
        });
    
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        return auth.post("/reset-password", email, axiosConfig);
    }
}

export default new AuthService();