import assignments from '../api/assignments.js';

class AssignmentsService {

    getAllAssignmentsCourse(courseId) {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        return assignments.get("/" + courseId, axiosConfig);
    }

    async submitFiles(files, userId, assignmentId) {
        let responses =[];

            //const files = await getFilePaths();
          
            for (const file of files) {
              const response = await this.submitFile(file, userId, assignmentId);
              responses = [...responses, response];
            }
            // updatePercentage(null);
            return Promise.resolve(responses);
    }

    submitFile(file, userId, assignmentId) {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
                'content-type': 'multipart/form-data',
                //...(authHeader())
            },
            //onUploadProgress: progressEvent => {console.log("" + (progressEvent.loaded / progressEvent.total) * 100); updatePercentage({filename:file.path, procentageValue: (progressEvent.loaded / progressEvent.total) * 100})}
        };
        // updatePercentage({filename:file.path, procentageValue: 0})
        let formData = new FormData();
        //formData.append("userId", 1);
        // formData.append("path", '');
    //    if(parentId) {
            formData.append("userId", userId);
    //    }
        formData.append("assignmentId", assignmentId);
        formData.append("file", file);
        return assignments.post("/", formData, axiosConfig);
    }

    delete(file) {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            },
        };
        
        return assignments.delete("/" + file.id, axiosConfig);
    }

    download(file) {
        return fetch( assignments.defaults.baseURL+ "/"+ file.id, {
            method: 'POST',
            headers: {
                // 'Content-Type': 'application/json'
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            },
        })
    }

    createAssignment(title, description, dueDate, courseId) {
        let classJSON = JSON.stringify({
            title: title,
            description: description,
            dueDate: dueDate,
            course: {id: courseId}
        });

        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        return assignments.post("/" + courseId + "/create", classJSON, axiosConfig);
    }
    
    getAssignmentById(assignmentId) {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        return assignments.get("/assignments/" + assignmentId, axiosConfig);
    
    }
}

export default new AssignmentsService();