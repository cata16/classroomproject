import assignmentDetails from '../api/assignmentDetails.js';

class AssignmentDetailsService {

    getAssignmentDetails(assignmentId, studentId) {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        return assignmentDetails.get("/" + assignmentId + "/" + studentId, axiosConfig);
    }

    gradeAssignment(studentAssignmentId, grade) {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        return assignmentDetails.post("/" + studentAssignmentId+"/addGrade", grade, axiosConfig);
    }
}

export default new AssignmentDetailsService();