import courses from '../api/courses.js';

class CoursesService {
    getAllCourses() {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        return courses.get("/", axiosConfig);
    }

    joinClass(classCode, studentId) {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        return courses.post("/" + classCode + "/join/" + studentId, axiosConfig);
    }

    createClass(className, teacherId) {
        let classJSON = JSON.stringify({
            name: className,
            teacher: {id: teacherId}
        });
    
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
    
        return courses.post("/", classJSON, axiosConfig);
    }

    getAllCoursesStudent(studentId) {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        return courses.get("/student/" + studentId, axiosConfig);
    }

    getAllCoursesTeacher(teacherId) {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        return courses.get("/teacher/" + teacherId, axiosConfig);
    }

    getAllCoursePeople(courseId) {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        return courses.get(courseId + "/people", axiosConfig);
    }

    getCourseById(courseId) {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        return courses.get("/" + courseId, axiosConfig);
    }

    leaveClass(courseId, studentId) {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
        };
        return courses.get("/" + courseId + "/" + studentId + "/leave", axiosConfig);
    }

}

export default new CoursesService();