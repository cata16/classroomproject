import student from "../api/profileStudent.js";

class StudentService {
  getProfileDetails(studentId) {
    let axiosConfig = {
      headers: {
        "Content-Type": "application/json;charset=UTF-8",
        "Access-Control-Allow-Origin": "*",
      },
    };
    return student.get("/" + studentId, axiosConfig);
  }
}

export default new StudentService();
