import teacher from "../api/profileTeacher.js";

class TeacherService {
  getProfileDetails(teacherId) {
    let axiosConfig = {
      headers: {
        "Content-Type": "application/json;charset=UTF-8",
        "Access-Control-Allow-Origin": "*",
      },
    };
    return teacher.get("/" + teacherId, axiosConfig);
  }
}

export default new TeacherService();
