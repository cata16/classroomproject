import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from "i18next-browser-languagedetector";
 
import { TRANSLATIONS_EN } from "./en/locales";
import { TRANSLATIONS_RO } from "./ro/locales";
i18n
 .use(LanguageDetector)
 .use(initReactI18next)
 .init({
   resources: {
     en: {
       translation: TRANSLATIONS_EN
     },
     ro: {
      translation: TRANSLATIONS_RO
    },
     fallbackLng: 'en',
   },
 });
 
export default i18n;