import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { Route, Switch } from "react-router";
import Header from "./widgets/Header";
import { urls } from "../../utils/utils";
import { withTranslation } from "react-i18next";
import CreateAssignmentModal from "./widgets/CreateAssignmentModal";
import AssignmentWidget from "./widgets/AssignmentWidget";
import {getAllAssignmentsCourse} from "../../actions/assignments"
import 'bootstrap/dist/css/bootstrap.min.css';  
import {Container ,Card,Row, Col, Button} from 'react-bootstrap'; 

class TeacherCourseAssignments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            assignments: [],
            students:[],
            openCreateAssignmentModal: false
        }
    }

    componentDidUpdate(prevProps) {
        if(this.props.courseId != prevProps.courseId) {
            this.props
			.getAllAssignmentsCourse(this.props.courseId
			)
			.then(() => {
                this.setState({
                    assignments: this.props.assignments
                })
				//this.redirectToStudentPage();
				console.log("Get all assignments student");
			}).catch(() => {
				console.log("CatchComponentReject");
			});
        }
    }

    componentDidMount() {
		//window.localStorage.clear();
        // const user = JSON.parse(localStorage.getItem('user'));
        // const courseId = this.props.match.params.id;
        // this.props.updateCurrentCourseId(courseId);
		// var pathname = window.location.href.split("/");
        // var courseId = pathname[pathname.length - 1];
        // this.props
		// 	.getAllAssignmentsCourse(this.props.courseId
		// 	)
		// 	.then(() => {
        //         this.setState({
        //             assignments: this.props.assignments
        //         })
		// 		//this.redirectToStudentPage();
		// 		console.log("Get all assignments student");
		// 	}).catch(() => {
		// 		console.log("CatchComponentReject");
		// 	});
        // this.setState({courseId: courseId})
	}

    renderAssignments = (assignments) => {
        return assignments.map(aAssignments => {
            return (
                <AssignmentWidget courseId={this.props.courseId} assignment ={aAssignments} history={this.props.history}/>
            )
        })
    }
    
    onCreateAssignmentClick() {
        this.setState({
            openCreateAssignmentModal: true
        })
    }

    handleCloseCreateAssignmentModal = () => {
        this.setState({
            openCreateAssignmentModal: false,
        })
    }

    render (){ 
        const { t } = this.props;
        return (    
            <div>
                <Container className="p-4">  
                <Button 
                type="button"
                className="btn-view"
                onClick={() => this.onCreateAssignmentClick()} variant="outlined">
                    {t("createAssignment")}
                </Button>
                <CreateAssignmentModal 
                    open={this.state.openCreateAssignmentModal}
                    handleClose={this.handleCloseCreateAssignmentModal}
                />

                <Row>
                {this.renderAssignments(this.state.assignments)}
                </Row>
                </Container>
            </div>
        )}

      
}
const mapStateToProps = (state) => {
	return {
        assignments: state.assignments.assignments,
        students: state.courses.students
	};
};

export default compose(
    connect(mapStateToProps, {getAllAssignmentsCourse}),
    withTranslation(),
)(TeacherCourseAssignments);