import { Component } from "react";
import logo from "../../../resources/logo.png";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { compose } from "redux";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";
import { logout } from "../../../actions/auth";
import LogoutIcon from "@mui/icons-material/Logout";
import Button from "@material-ui/core/Button";
import ListItemText from "@material-ui/core/ListItemText";
import { urls } from "../../../utils/utils";
import React, { useState } from "react";
import CreateClassModal from "./CreateClassModal";
// import StudentProfilePage from "../StudentProfilePage";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";

import "./Header.css";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openCreateClassModal: false,
      modalVisible: false,
    };
  }

  logout = () => {
    this.props.history.push(urls.login);
    this.props.logout();
  };

  redirectToProfile = () => {
    const user = JSON.parse(localStorage.getItem("user"));
    this.props.history.push(urls.profileTeacher + "/" + user.id);
    window.location.reload();
  };

  openModal = () => {
    this.setState({
      modalVisible: !this.state.modalVisible,
      openCreateClassModal: true,
    });
    //setShowModal(prev => !prev);
  };

  updateModalVisibility = (v) => {
    this.setState({ modalVisible: v });
  };

  handleCloseCreateClassModal = () => {
    this.setState({
      openCreateClassModal: false,
    });
  };

  redirectToClasswork = () => {
    // this.props.history.goBack();

		// var pathname = window.location.href.split("/");
    // var courseId = pathname[pathname.length - 2]; 
    this.props.history.push(urls.class + "/" + this.props.courseId);
    window.location.reload();
  };


  redirectToCoursePeople = () => {
    // var indexOfClassPeople = this.props.history.location.pathname.indexOf(urls.classPeople)
    // var newUrl = this.props.history.location.pathname
    // if(indexOfClassPeople != -1) {
    //   newUrl = this.props.history.location.pathname.substring(0, indexOfClassPeople);
    // }
    this.props.history.push(urls.class + "/" + this.props.courseId + urls.classPeople);
    window.location.reload();
  }

  renderButtons() {
    const { t } = this.props;

    return (<div>
              <Button
                variant="contained"
                color="primary"
                className="header-button"
                onClick={this.openModal}
              >
                {t("createClass")}
              </Button>
              <Button
                variant="contained"
                color="primary"
                className="header-button"
                onClick={() => this.redirectToProfile()}
              >
                {t("profile")}
              </Button></div>);
  }

  renderClassButtons() {
    const { t } = this.props;
    return (<div>
              <Button
                variant="contained"
                color="primary"
                className="header-button"
                onClick={() => this.redirectToClasswork()}
              >
                {t("classwork")}
              </Button>
              <Button
                variant="contained"
                color="primary"
                className="header-button"
                onClick={() => this.redirectToCoursePeople()}
              >
                {t("people")}
              </Button>
              <Button
                variant="contained"
                color="primary"
                className="header-button"
                onClick={() => this.redirectToProfile()}
              >
                {t("edit")}
              </Button>
    </div>);
  }

  render() {
    const { t } = this.props;

    return (
      <div className="header-container">
        <AppBar position="static">
          <Toolbar className="header">
            <Link to={urls.login}>
              <img className="header-logo" src={logo} height="70" />
            </Link>
            <div className="header-buttons">
              {this.props.url == urls.teacher? 
                this.renderButtons(): null
              }

              {this.props.url == urls.class? 
                this.renderClassButtons(): null
              }
              
              <CreateClassModal
                open={this.state.openCreateClassModal}
                handleClose={this.handleCloseCreateClassModal}
                on
              />
            </div>
            {/* {this.props.user && this.props.user.username ? */}
            <div className="header-content">
              {/*<div className="header-name">
                <span>
                  {t("welcome") +
                    (this.props.user.email
                      ? ", " + this.props.user.email
                      : "")}
                </span>
                    </div>*/}

              <div className="header-action">
                <Button
                  variant="contained"
                  color="primary"
                  className="form-button"
                  onClick={() => this.logout()}>
                  {t("logout")}
                </Button> 
                {/* <span className="link">
                        <Link to={urls.settings} color="inherit">{t('settings')}</Link>
                      </span> */}
                {/*<span className="link">
                  <Link to="/" color="inherit" onClick={this.logout}>
                    <LogoutIcon />
                  </Link>
                    </span>*/}
                {/* <UserSettings/> */}
              </div>
            </div>
            {/* : null } */}
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.auth.user,
  };
};
export default compose(
  connect(mapStateToProps, { logout }),
  withTranslation()
)(Header);
