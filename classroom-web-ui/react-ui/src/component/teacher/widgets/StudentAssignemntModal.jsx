import React, { useState } from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import { MuiPickersUtilsProvider,DateTimePicker } from "@material-ui/pickers";
import {connect} from 'react-redux';
import DateFnsUtils from '@date-io/date-fns';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { gradeAssignment } from "../../../actions/assignmentDetails";
import FileListView from '../../commons/widgets/FileListView';

class StudentAssignemntModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {           
            dialogTitle: 'Review Assignment',
            studentAssignment: null,
        }
    }

    componentDidMount() {
        this.setState({studentAssignment: this.props.studentAssignment})
    }

    componentDidUpdate(prevProps) {
        if(this.props.studentAssignment != prevProps.studentAssignment) {
            this.setState({studentAssignment: this.props.studentAssignment})
        }
    }

    handleClose = () => {
        this.props.handleClose();
        this.resetState();
        window.location.reload();
    }


    resetState() {
        this.setState({
        });
    }

    handleSave = () => {
        this.props.gradeAssignment(this.state.studentAssignment.id, this.state.studentAssignment.grade);
        window.location.reload();
        console.log("save");
    }

    render() {
        return (
            <Dialog onClose={this.handleClose} open={this.props.open} disableEnforceFocus
                fullWidth
                maxWidth="m"
                    >
                <DialogTitle id="simple-dialog-title">
                    {this.state.dialogTitle}
                </DialogTitle>
                <DialogContent  style={{height:'80vh'}}>
                {this.state.studentAssignment?
                    <>
                    <TextField
                            id='studentName'
                            value={this.state.studentAssignment.student.fullName}
                            type='text'
                            label='Student Name'
                            variant='outlined'
                            margin='normal'
                            inputProps={
                                { readOnly: true, }
                            }
                        />
                     <TextField
                            id='grade'
                            onChange={e => this.setState({ studentAssignment: {...this.state.studentAssignment, grade: e.target.value }})}
                            type='text'
                            label='Grade'
                            variant='outlined'
                            margin='normal'
                            value={this.state.studentAssignment.grade}
                        />
                     <TextField
                            id='assignmentTitle'
                            type='text'
                            label='Status'
                            variant='outlined'
                            margin='normal'
                            value={this.state.studentAssignment.status}
                            inputProps={
                                { readOnly: true, }
                            }
                        />
                    <FileListView canDelete={false} files={this.state.studentAssignment.files}/>  

                    </>
                        :null}
                </DialogContent>
                <DialogActions >
                <Button onClick={this.handleClose} variant="outlined">
                        Cancel
                    </Button>
                    <Button onClick={this.handleSave} color="primary" variant="contained">
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
            
        );
    }
}

const mapStateToProps = (state) => {
    return {
    }
}
export default connect(mapStateToProps, { gradeAssignment })
(StudentAssignemntModal);