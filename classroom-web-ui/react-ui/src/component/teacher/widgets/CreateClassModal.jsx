import React, { useState } from "react";
import { compose } from "redux";
import { withTranslation } from "react-i18next";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import { createClass } from "../../../actions/courses";
import { urls } from "../../../utils/utils";

class CreateClassModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = { className: "" };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleCreateClass() {
    const user = JSON.parse(localStorage.getItem("user"));
    this.props.createClass(this.state.className, user.id);
    window.location.reload();
  }

  handleChange(e) {
    this.setState({ className: e.target.value });
  }

  handleSubmit(event) {
    alert("You created the class!");
    event.preventDefault();
  }

  renderCreate() {
    return (
      <div>
        {/* <form onSubmit={this.handleSubmit}>
                    <label>
                    Code:
                    <input type="text" value={this.state.classCode} onChange={this.handleChange} />        </label>
                    <input type="submit" value="Submit" />
                </form> */}

        <p>Enter the class name.</p>
        <input
          ref={(inputarea) => (this.inputArea = inputarea)}
          value={this.state.className}
          onChange={this.handleChange}
          placeholder="Class name"
        ></input>
      </div>
    );
  }

  render() {
    const { t } = this.props;
    return (
      <Dialog
        onClose={this.props.handleClose}
        open={this.props.open}
        disableEnforceFocus
        maxWidth="md"
      >
        <DialogTitle id="simple-dialog-title">
          <p>Create class</p>
        </DialogTitle>
        <DialogContent style={{ height: "20vh" }}>
          {this.renderCreate()}
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.handleClose} variant="outlined">
            {t("cancel")}
          </Button>
          <Button
            onClick={() => this.handleCreateClass()}
            color="primary"
            variant="contained"
          >
            {t("create")}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};
export default compose(
  connect(mapStateToProps, { createClass }),
  withTranslation()
)(CreateClassModal);
