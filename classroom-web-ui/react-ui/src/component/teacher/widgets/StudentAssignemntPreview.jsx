import React, { useState } from "react";
import { compose } from "redux";
import { withTranslation } from "react-i18next";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import { createClass } from "../../../actions/courses";
import { urls } from "../../../utils/utils";
import StudentAssignemntModal from "./StudentAssignemntModal";

class StudentAssignemntPreview extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      openReviewAssignmentModal: false,
      studentAssignment: null
     };
  }

  componentDidUpdate(prevProps) {
    if(this.props.studentAssignment != prevProps.studentAssignment) {
      this.setState({studentAssignment: this.props.studentAssignment})
    }
  }

  reviewAssignmentClicked() {
    this.setState({openReviewAssignmentModal: true})
  }
  
  handleCloseReviewAssignmentModal = () => {
    this.setState({openReviewAssignmentModal: false})
  }

  render() {
    
    return (
      <div>
        {this.props.studentAssignment? 
          <>
            <div>{"Student: " + this.props.studentAssignment.student.fullName}</div>
            <div>{"Grade: " + this.props.studentAssignment.grade}</div>
            <div>{"Status: " + this.props.studentAssignment.status}</div>
            <Button
                variant="contained"
                color="primary"
                className="header-button"
                onClick={() => this.reviewAssignmentClicked()}
              >
                Review
            </Button>
          </>:
        null}
        <StudentAssignemntModal 
            studentAssignment={this.props.studentAssignment} 
            open={this.state.openReviewAssignmentModal}
            handleClose={this.handleCloseReviewAssignmentModal}/>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};
export default compose(
  connect(mapStateToProps, { createClass }),
  withTranslation()
)(StudentAssignemntPreview);
