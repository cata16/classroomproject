import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";
import 'bootstrap/dist/css/bootstrap.min.css';  
import {Container , Card, Row, Col, Button} from 'react-bootstrap'; 

import "./ClassWidget.css"
import { urls } from "../../../utils/utils";

class ClassWidget extends Component {

    openClassPage() {
        this.props.history.push(urls.class + '/' + this.props.course.id);
		window.location.reload();
    } 

    render () {
        return (
            <Card className="class-widget" >
                <div>{this.props.course.name}</div>
                {/*<div>Teacher name {this.props.course.teacherId}</div>*/}
                <br></br>
                <Button 
                type="button" 
                className="btn-view"
                onClick={() => this.openClassPage()}
                >
                    View
                </Button>
            </Card>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
	return {
		user: state.auth.user
	};
};
export default compose(
	connect(mapStateToProps, {}),
	withTranslation()
)(ClassWidget);