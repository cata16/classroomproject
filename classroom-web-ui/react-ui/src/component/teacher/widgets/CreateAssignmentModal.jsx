import React, { useState } from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import { MuiPickersUtilsProvider,DateTimePicker } from "@material-ui/pickers";
import {connect} from 'react-redux';
import DateFnsUtils from '@date-io/date-fns';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { createAssignment } from "../../../actions/assignments";
import {Container , Card, Row, Col} from 'react-bootstrap'; 

import "./CreateAssignmentModal.css";

class CreateAssignmentModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {           
            dialogTitle: 'Create Assignment',
            title: '',
            description: '',
            dueDate: new Date(),
        }
    }

    handleClose = () => {
        this.props.handleClose();
        this.resetState();
        window.location.reload();
    }


    resetState() {
        this.setState({
            title: '',
            description: '',
            dueDate: new Date(),
        });
    }

    handleDateChange = (newDate) => {
        this.setState({dueDate: newDate});
    }

    handleSave = () => {
        const user = JSON.parse(localStorage.getItem("user"));
        var pathname = window.location.href.split("/");
        var courseId = pathname[pathname.length - 1]; 
        this.props.createAssignment(this.state.title, this.state.description, this.state.dueDate, courseId);
        window.location.reload();
        console.log("save");

    }

    render() {
        return (
            <Dialog onClose={this.handleClose} open={this.props.open} disableEnforceFocus
                fullWidth
                maxWidth="xl"
                    >
                <DialogTitle id="simple-dialog-title">
                    {this.state.dialogTitle}
                </DialogTitle>
                {/* , display: "flex" */}
                <DialogContent  style={{height:'80vh' }}>
                    <TextField
                            id='assignmentTitle'
                            onChange={e => this.setState({ title: e.target.value })}
                            type='text'
                            label='Title'
                            variant='outlined'
                            margin='normal'
                        />
                    <TextField
                            id='assignmentDescription'
                            onChange={e => this.setState({ description: e.target.value })}
                            type='text'
                            label='Description'
                            variant='outlined'
                            margin='normal'
                        />
                    {/* <Col></Col> */}
                    {/* style={{ marginLeft: "auto" }} */}
                    <MuiPickersUtilsProvider utils={DateFnsUtils} >
                    <DateTimePicker
                        id="dueDate"
                        label="Due date"
                        inputVariant="outlined"
                        value={this.state.dueDate}
                        onChange={this.handleDateChange}
                        format="yyyy/MM/dd HH:mm"
                    />
                    </MuiPickersUtilsProvider>
                </DialogContent>
                <DialogActions >
                <Button onClick={this.handleClose} variant="outlined">
                        Cancel
                    </Button>
                    <Button onClick={this.handleSave} color="primary" variant="contained">
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
            
        );
    }
}

const mapStateToProps = (state) => {
    return {
    }
}
export default connect(mapStateToProps, { createAssignment })
(CreateAssignmentModal);