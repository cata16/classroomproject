import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { Route, Switch } from "react-router";
import { urls } from "../../utils/utils";
import { withTranslation } from "react-i18next";
import Button from "@material-ui/core/Button";
import { getProfileDetails } from "../../actions/profileTeacher";

import 'bootstrap/dist/css/bootstrap.min.css';
// import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import "./TeacherProfilePage.css";

class TeacherProfilePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      teacher: [],
      fullName: '',
      university: '',
      department: '',
      phoneNumber: '',
      email: '',
      oldPassword: '',
      newPassword: '',
      confirmNewPassword: '',
      oldPasswordRequired: false,
      newPasswordRequired: false,
      confirmNewPasswordRequired: false,
    };
    this.handleChangeFullName = this.handleChangeFullName.bind(this);
    this.handleChangeUniversity = this.handleChangeUniversity.bind(this);
    this.handleChangeDepartment = this.handleChangeDepartment.bind(this);
    this.handleChangePhoneNumber = this.handleChangePhoneNumber.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
  }

  componentDidMount() {
    //window.localStorage.clear();
    const user = JSON.parse(localStorage.getItem("user"));
    this.props
      .getProfileDetails(user.id)
      .then(() => {
        this.setState({
          teacher: this.props.teacher,
        });
        console.log("See profile details");
      })
      .catch(() => {
        console.log("CatchComponentReject");
      });
  }

  handleChangeFullName(e) {    
    this.setState({fullName: e.target.value});  
  }

  handleChangeUniversity(e) {    
    this.setState({university: e.target.value});  
  }

  handleChangeDepartment(e) {    
    this.setState({department: e.target.value});  
  }

  handleChangePhoneNumber(e) {    
    this.setState({phoneNumber: e.target.value});  
  }

  handleChangeEmail(e) {    
    this.setState({email: e.target.value});  
  }

  render() {
    return (
      <div>     
        <div class="container rounded bg-white mt-5 mb-5">
            <div class="row">
                <div class="col-md-5 border-right">
                    <div class="p-3 py-5">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <h4 class="text-right">Profile Settings</h4>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12"><label class="labels">Full Name</label><input type="text" class="form-control" placeholder={this.props.teacher.fullName} value={this.state.fullName}  onChange={this.handleChangeFullName}></input></div>
                            <div class="col-md-12"><label class="labels">University</label><input type="text" class="form-control" placeholder={this.props.teacher.university} value={this.state.university}  onChange={this.handleChangeUniversity}></input></div>
                            <div class="col-md-12"><label class="labels">Department</label><input type="text" class="form-control" placeholder={this.props.teacher.department} value={this.state.department}  onChange={this.handleChangeDepartment}></input></div>
                            <div class="col-md-12"><label class="labels">Phone Number</label><input type="text" class="form-control" placeholder={this.props.teacher.phoneNumber} value={this.state.phoneNumber}  onChange={this.handleChangePhoneNumber}></input></div>
                            <div class="col-md-12"><label class="labels">Email Address</label><input type="text" class="form-control" placeholder={this.props.teacher.email} value={this.state.email}  onChange={this.handleChangeEmail}></input></div>
                        </div>
                        <div class="mt-5 text-center"><Button class="btn btn-primary profile-button">Change Request</Button></div>
                    </div>
                    <div class="p-3 py-5">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <h4 class="text-right">Change Password</h4>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12"><label class="labels">Old Password</label><input type="password" class="form-control" placeholder="" value={this.state.oldPassword} 
                            onChange={(e) =>
                              this.setState({
                                oldPassword: e.target.value,
                                oldPasswordRequired: false,
                              })  
									          }
                            ></input></div>
                            <div class="col-md-12"><label class="labels">New Password</label><input type="password" class="form-control" placeholder="" value={this.state.newPassword} 
                            onChange={(e) =>
                              this.setState({
                                newPassword: e.target.value,
                                newPasswordRequired: false,
                              })  
									          }
                            ></input></div>
                            <div class="col-md-12"><label class="labels">Confirm New Password</label><input type="password" class="form-control" placeholder="" value={this.state.confirmNewPassword} 
                            onChange={(e) =>
                              this.setState({
                                confirmNewPassword: e.target.value,
                                confirmNewPasswordRequired: false,
                              })  
									          }
                            ></input></div>
                        </div>
                        <div class="mt-5 text-center"><Button class="btn btn-primary profile-button">Change Password</Button></div>
                    </div>
                </div>
            </div>
        </div>

        
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { teacher: state.teacher.teacher };
};

export default compose(
  connect(mapStateToProps, { getProfileDetails }),
  withTranslation()
)(TeacherProfilePage);
