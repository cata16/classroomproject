import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { Route, Switch } from "react-router";
import { urls } from "../../utils/utils";
import { withTranslation } from "react-i18next";
import Header from "./widgets/Header";
import TeacherCoursesPage from "./TeacherCoursesPage";
import {history} from '../../helpers/history';
//import { HashRouter } from 'react-router-dom';

class TeacherHomeContainer extends Component {
  render() {
    return (
      <div>
        <Header {...this.props}  url={urls.teacher} />
        {/* <div>Teacher Home Page </div> */}
        <div className="home-content">
          <div className="home-data-container">
            <Switch>
              <Route path={urls.teacher} render={(props) => <TeacherCoursesPage {...props} history={this.props.history} />}/>
            </Switch>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {};
};

export default compose(
  connect(mapStateToProps, {}),
  withTranslation()
)(TeacherHomeContainer);
