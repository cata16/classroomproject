import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { Route, Switch } from "react-router";
import Header from "./widgets/Header";
import { urls } from "../../utils/utils";
import { withTranslation } from "react-i18next";
import {getAllCoursePeople} from "../../actions/courses";
import { leaveClass } from "../../actions/courses";
import 'bootstrap/dist/css/bootstrap.min.css';  
// import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import {Container ,Card,Row, Col, Button} from 'react-bootstrap'; 
import { withRouter } from "react-router";

class TeacherCoursesPeople extends Component {
    constructor(props) {
        super(props);
        this.state = {
            students:[]
        }
    }

    // componentDidMount() {
    //     const courseId = this.props.match.params.id;
    //     this.props.updateCurrentCourseId(courseId);
    // }

    componentDidUpdate(prevProps, prevState){
        if(this.props.course!==prevProps.course){
            this.setState({course: this.props.course}, () => {this.getCoursePeople(this.state.course.id)});
       }
     }

    getCoursePeople = (courseId) => {
        this.props
			.getAllCoursePeople(courseId)
			.then(() => {
                this.setState({
                    students: this.props.students
                })
				//this.redirectToStudentPage();
				console.log("Get all courses teacher");
			}).catch(() => {
				console.log("CatchComponentReject");
			});
    }

    redirectToRemove = (studentId) => {
        // window.location.reload();
        var proceed = window.confirm("Are you sure you want to proceed?");
        if (proceed) {
          //proceed 
        //   var pathname = window.location.href.split("/");
        //   var aCourseId = pathname[pathname.length - 2];
          this.props.leaveClass(this.state.course.id, studentId);
        //   this.props.leaveClass(aCourseId, studentId);
    
          window.location.reload();
        } 
        else {
          //don't proceed
        }
      };

    redirectToAdd = () => {
        window.location.reload();
    };

    renderStudents = () => {
        return (
            this.state.students.map(student => 
            <div>
                
                <br></br>
                <div style={{ display: "flex" }}>
                    {student.fullName} 
                    
                    <Button
                        // style={{ marginLeft: "auto", backgroundColor: "orange" }}
                        style={{ marginLeft: "auto" }}
                        type="button"
                        onClick={() => this.redirectToRemove(student.id)}
                        >
                            Remove
                    </Button>
                </div>
                
                <hr />
                <br></br>   
            </div>
            )
        )
    }

    render (){ 
        const { t } = this.props;
        return (    
            <div>
                <Container className="p-4">  
                <Row>
                    <h4>Teacher</h4> 
                    <hr></hr>
                    <div>
                        {this.state.course && this.state.course.teacher? this.state.course.teacher.fullName : ""}
                    </div> 
                </Row>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <Row>
                    <h4>Classmates</h4> 
                    <hr></hr>
                    <br></br>
                </Row>

                <Button
                        style={{ marginLeft: "auto" }}
                        type="button"
                        onClick={() => this.redirectToAdd()}
                        >
                            Add
                </Button>

                {this.renderStudents()}
                
                </Container>

                {/* <div>{t("teacher")}</div>
                <div>{this.state.course && this.state.course.teacher? this.state.course.teacher.fullName : ""}</div>    
                <div>{t("students")}</div>
                {this.renderStudents()}
                Student course people */}
            </div>
        )}

      
}
const mapStateToProps = (state) => {
	return {
        students: state.courses.students
	};
};

export default compose(
    connect(mapStateToProps, {getAllCoursePeople, leaveClass}),
    withTranslation(),
    withRouter
)(TeacherCoursesPeople);