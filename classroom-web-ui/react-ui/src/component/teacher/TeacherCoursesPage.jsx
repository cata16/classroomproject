import React, { Component } from "react";
import { combineReducers, compose } from "redux";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";
import ClassWidget from "./widgets/ClassWidget";
import Grid from "@material-ui/core/Grid";
import {getAllCourses} from "../../actions/courses"
import {getAllCoursesTeacher} from "../../actions/courses"
import { useSSR } from "react-i18next";
import 'bootstrap/dist/css/bootstrap.min.css';  
import {Container ,Card,Row, Col, Button} from 'react-bootstrap'; 

class TeacherCoursesPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            courses: []
        }
    }
      
    componentDidMount() {
		//window.localStorage.clear();
        const user = JSON.parse(localStorage.getItem('user'));
		this.props
			.getAllCoursesTeacher(user.id
			)
			.then(() => {
                this.setState({
                    courses: this.props.courses
                })
				console.log("Get all courses teacher");
			}).catch(() => {
				console.log("CatchComponentReject");
			});
	}

    renderCourses = (courses) => {
        return courses.map(aCourse => {
            return (
                <ClassWidget course ={aCourse} history={this.props.history}/>
            )
        })
    }

    render (){ 
        return (    
            <div>
                <Container className="p-4">  
                <Row>
                {this.renderCourses(this.state.courses)}
                </Row>
                </Container>  
            </div>
        );
    }
}
const mapStateToProps = (state) => {
	return {
        courses: state.courses.courses
	};
};

export default compose(
    connect(mapStateToProps, {getAllCoursesTeacher}),
)(TeacherCoursesPage);