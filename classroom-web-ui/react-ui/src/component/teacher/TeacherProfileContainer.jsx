import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { Route, Switch } from "react-router";
import { urls } from "../../utils/utils";
import { withTranslation } from "react-i18next";
import Header from "./widgets/Header";
import TeacherProfilePage from "./TeacherProfilePage";

class TeacherProfileContainer extends Component {
  render() {
    return (
      <div>
        <Header {...this.props} url={urls.teacher} />
        <div className="profile-content">
          <div className="profile-data-container">
            <Switch>
              <Route path={urls.profileTeacher} render={(props) => <TeacherProfilePage {...props} history={this.props.history} />}/>
            </Switch>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {};
};

export default compose(
  connect(mapStateToProps, {}),
  withTranslation()
)(TeacherProfileContainer);
