import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { Route, Switch } from "react-router";
import Header from "./widgets/Header";
import { urls } from "../../utils/utils";
import TeacherCoursePeople from "./TeacherCoursePeople";
import {getAssignmentById} from "../../actions/assignments";
import { withTranslation } from "react-i18next";
import TeacherCourseAssignments from './TeacherCourseAssignments';
import { withRouter } from "react-router";
import StudentAssignemntPreview from './widgets/StudentAssignemntPreview';
import {Container , Card, Row, Col} from 'react-bootstrap'; 

class TeacherAssignments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            assignment: {},
            courseId:''
        }
    }

    componentDidMount() {
        const assignmentId = this.props.match.params.assignmentId;
        this.props.getAssignmentById(assignmentId)
        .then(() => {
            this.setState({
                assignment: this.props.assignment
            })
            
        }).catch(() => {
            console.log("CatchComponentReject");
        });
    }

    renderStudentsAssignments(studentAssignments) {
        return (studentAssignments?
        studentAssignments.map(studentAssignment => <StudentAssignemntPreview studentAssignment={studentAssignment} />) 
        : null)
    }

    render(){
        return(
            <div>
                <Container className="p-4">  
                <Row>
                {/* Assignment Title */}
                <h4> {this.state.assignment.title? this.state.assignment.title : null}</h4>
                <br></br>
                <br></br>
                <div></div>
                
                <hr />
                {/* Description */}
                <p class="description"> {this.state.assignment.description? this.state.assignment.description : null}</p>
                <div></div>
                </Row>
                {this.renderStudentsAssignments(this.state.assignment.studentAssignments)}
                </Container>
                {/* {this.state.assignment.title? this.state.assignment.title + this.state.assignment.description : null} */}
                {/* {this.renderStudentsAssignemtns(this.state.assignment.studentAssignments)} */}
            </div>
        )
    }
}
const mapStateToProps = (state) => {
	return {
        assignment: state.assignments.currentAssignment
	};
};

export default compose(
    connect(mapStateToProps, {getAssignmentById}),
    withTranslation(),
    withRouter
)(TeacherAssignments);