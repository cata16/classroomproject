import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { Route, Switch } from "react-router";
import Header from "./widgets/Header";
import { urls } from "../../utils/utils";
import TeacherCoursePeople from "./TeacherCoursePeople";
import {getCourseById} from "../../actions/courses";
import { withTranslation } from "react-i18next";
import TeacherCourseAssignments from './TeacherCourseAssignments';
import { withRouter } from "react-router";
import TeacherAssignments from "./TeacherAssignments";

class TeacherCourse extends Component {
    constructor(props) {
        super(props);
        this.state = {
            courses: [],
            courseId:''
        }
    }

    componentDidMount() {
        const courseId = this.props.match.params.id;
        this.updateCurrentCourseId(courseId);
    //     // var pathname = this.props.history.location.pathname;
    //     // var classStartIndex = pathname.indexOf(urls.class) + urls.class.length + 1;
    //     // var nextSeparatorIndex = pathname.substring(classStartIndex).indexOf('/')
    //     // var courseId = '';
    //     // if(nextSeparatorIndex != -1) {
    //     //     courseId = pathname.substring(classStartIndex).substring(0, nextSeparatorIndex);
    //     // } else {
    //     //     courseId = pathname.substring(classStartIndex);
    //     // }
        this.props.getCourseById(courseId)
        .then(() => {
            this.setState({
                course: this.props.course
            })
            
        }).catch(() => {
            console.log("CatchComponentReject");
        });
    }

    updateCurrentCourseId = (currentCourseId) => {
        this.setState({courseId: currentCourseId});
    }

    render (){ 
        const { t } = this.props;
        return (    
            <div>
                <Header {...this.props} url={urls.class} courseId={this.state.courseId} />
                
                {/* <div>Teacher Course</div> */}
                <Switch>
                    <Route path={urls.class+ "/:id" + urls.assignment + "/:assignmentId"} render={(props) => <TeacherAssignments {...props} courseId={this.state.courseId} course={this.state.course}/>}/>
                    <Route path={urls.class+ "/:id" + urls.classPeople} render={(props) => <TeacherCoursePeople {...props} courseId={this.state.courseId} course={this.state.course}/>}/>
                    <Route path={urls.class+ "/:id"} render={(props) => <TeacherCourseAssignments {...props} courseId={this.state.courseId} course={this.state.course}/>}/>
                </Switch>
                
                  
            </div>
        )}

      
}
const mapStateToProps = (state) => {
	return {
        courses: state.courses.courses,
        course: state.courses.course
	};
};

export default compose(
    connect(mapStateToProps, {getCourseById}),
    withTranslation(),
    withRouter
)(TeacherCourse);