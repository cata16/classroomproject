import React, { Component } from "react";
import { combineReducers, compose } from "redux";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";
import AssignmentWidget from "./widgets/AssignmentWidget";
import Grid from "@material-ui/core/Grid";
import {getAllAssignmentsCourse} from "../../actions/assignments"
import { useSSR } from "react-i18next";
import 'bootstrap/dist/css/bootstrap.min.css';  
import {Container ,Card,Row, Col, Button} from 'react-bootstrap'; 
import { withRouter } from "react-router";

class StudentAssignmentsPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            assignments: []
        }
    }
      
    componentDidUpdate(prevProps) {
        if(this.props.courseId != prevProps.courseId) {
            this.props
			.getAllAssignmentsCourse(this.props.courseId
			)
			.then(() => {
                this.setState({
                    assignments: this.props.assignments
                })
				//this.redirectToStudentPage();
				console.log("Get all assignments student");
			}).catch(() => {
				console.log("CatchComponentReject");
			});
        }
    }

    // componentDidMount() {
        // const courseId = this.props.match.params.id;
        // this.props.updateCurrentCourseId(courseId);
        
        // var pathname = window.location.href.split("/");
        // var courseId = pathname[pathname.length - 1];
        
        // this.props
		// 	.getAllAssignmentsCourse(courseId
		// 	)
		// 	.then(() => {
        //         this.setState({
        //             assignments: this.props.assignments
        //         })
		// 		//this.redirectToStudentPage();
		// 		console.log("Get all assignments student");
		// 	}).catch(() => {
		// 		console.log("CatchComponentReject");
		// 	});
        // this.setState({courseId: courseId})
	// }

    componentDidMount() {
        this.props
			.getAllAssignmentsCourse(this.props.courseId
			)
			.then(() => {
                this.setState({
                    assignments: this.props.assignments
                })
				//this.redirectToStudentPage();
				console.log("Get all assignments student");
			}).catch(() => {
				console.log("CatchComponentReject");
			});
	}

    renderAssignments = (assignments) => {
        return assignments.map(aAssignments => {
            return (
                <AssignmentWidget courseId={this.props.courseId} assignment ={aAssignments} history={this.props.history}/>
            )
        })
    }

    render (){ 
        const { t } = this.props;
        return (    
            <div>
                <Container className="p-4">  
                <Row>
                {this.renderAssignments(this.state.assignments)}
                </Row>
                </Container>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
	return {
        assignments: state.assignments.assignments
	};
};

export default compose(
    connect(mapStateToProps, {getAllAssignmentsCourse}),
    withTranslation(),
)(StudentAssignmentsPage);