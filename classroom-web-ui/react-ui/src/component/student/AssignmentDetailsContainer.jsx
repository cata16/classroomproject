import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { Route, Switch } from "react-router";
import { urls } from "../../utils/utils";
import { withTranslation } from "react-i18next";
import Header from "./widgets/Header";
import  AssignmentDetailsPage from "./AssignmentDetailsPage";
import { withRouter } from "react-router";

class AssignmentDetailsContainer extends Component {
  componentDidMount() {
        const courseId = this.props.match.params.id;
        this.props.updateCurrentCourseId(courseId);
  }

  render() {
    return (
      <div>
        {/* <Header {...this.props} url={urls.class} /> */}
        {/*<div>Student Course Page </div>*/}
        <div className="assignment-content">
          <div className="assignment-data-container">
          <AssignmentDetailsPage {...this.props} history={this.props.history} />
            {/* <Switch>
              <Route path={urls.assignmentDetails} render={(props) => <AssignmentDetailsPage {...props} history={this.props.history} />} />
            </Switch> */}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {};
};

export default compose(
  connect(mapStateToProps, {}),
  withTranslation(),
  withRouter
)(AssignmentDetailsContainer);
