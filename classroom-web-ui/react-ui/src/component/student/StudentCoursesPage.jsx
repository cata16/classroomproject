import React, { Component } from "react";
import { combineReducers, compose } from "redux";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";
import ClassWidget from "./widgets/ClassWidget";
import Grid from "@material-ui/core/Grid";
import {getAllCourses} from "../../actions/courses"
import {getAllCoursesStudent} from "../../actions/courses"
import { useSSR } from "react-i18next";
import 'bootstrap/dist/css/bootstrap.min.css';  
import {Container ,Card,Row, Col, Button} from 'react-bootstrap'; 

class StudentCoursesPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            courses: []
        }
    }
      
    componentDidMount() {
		//window.localStorage.clear();
        const user = JSON.parse(localStorage.getItem('user'));
		this.props
			.getAllCoursesStudent(user.id
			)
			.then(() => {
                this.setState({
                    courses: this.props.courses
                })
				//this.redirectToStudentPage();
				console.log("Get all courses student");
			}).catch(() => {
				console.log("CatchComponentReject");
			});
	}

    renderCourses = (courses) => {
        return courses.map(aCourse => {
            return (
                <ClassWidget course ={aCourse} history={this.props.history}/>
            )
        })
    }

    render (){ 
        return (    
            <div>
                <Container className="p-4">  
                <Row>
                {this.renderCourses(this.state.courses)}
                </Row>
                </Container>
                
                { /*
                <Grid container spacing={2}>
                    <Grid item xs={3}>   
                        <ClassWidget/>
                    </Grid>
                    
                    <Grid item xs={3}>    
                        <ClassWidget/>
                    </Grid>
                    
                    <Grid item xs={3}>
                        <ClassWidget/>
                    </Grid>
                    
                    <Grid item xs={3}>
                        <ClassWidget/>
                    </Grid>
                </Grid> 
                */}

                {/*
                <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
                    {Array.from(Array(3)).map((_, index) => (
                        <Grid item xs={2} sm={4} md={4} key={index}>
                            {this.renderCourses(this.state.courses)}
                        </Grid>
                    ))}
                </Grid>
                */}
                
            </div>
        );
    }
}
const mapStateToProps = (state) => {
	return {
        courses: state.courses.courses
	};
};

export default compose(
    connect(mapStateToProps, {getAllCoursesStudent}),
    withTranslation()
)(StudentCoursesPage);