import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { Route, Switch } from "react-router";
import { urls } from "../../utils/utils";
import { withTranslation } from "react-i18next";
import Header from "./widgets/Header";
import {getCourseById} from "../../actions/courses";
import { withRouter } from "react-router";
import StudentAssignmentsPage from "./StudentAssignmentsPage";
import StudentCoursePeople from './StudentCoursePeople';
import AssignmentDetailsContainer from "./AssignmentDetailsContainer";

class StudentCourseContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      courses: [],
      courseId:''
    }
  }
  
  componentDidMount() {
    const courseId = this.props.match.params.id;
    this.updateCurrentCourseId(courseId);
    this.props.getCourseById(courseId)
    .then(() => {
        this.setState({
            course: this.props.course
        })
        
    }).catch(() => {
        console.log("CatchComponentReject");
    });
 }

 updateCurrentCourseId = (currentCourseId) => {
  this.setState({courseId: currentCourseId});
}


  render() {
    const { t } = this.props;
    return (
      <div>
        <Header {...this.props} url={urls.classStudent} courseId={this.state.courseId}/>
        {/*<div>Student Course Page </div>*/}
        <Switch>
            <Route path={urls.classStudent + "/:id"} render={(props) => <StudentAssignmentsPage {...props} courseId={this.state.courseId} course={this.state.course}/>} />
        </Switch>
        <div className="course-content">
          <div className="course-data-container">
          
           {/*} <Switch>
                    <Route path={urls.class + "/:id" + urls.assignment + "/:assignmentId"} render={(props) => <AssignmentDetailsContainer {...props} history={this.props.history} updateCurrentCourseId={this.updateCurrentCourseId}/> }/>
                    <Route path={urls.class+ "/:id" + urls.classPeople} render={(props) => <StudentCoursePeople {...props} history={this.props.history} updateCurrentCourseId={this.updateCurrentCourseId}/>}/>
                    <Route path={urls.class+ "/:id"} render={(props) => <StudentAssignmentsPage {...props} updateCurrentCourseId={this.updateCurrentCourseId} history={this.props.history} />}/>
                </Switch>
            {/* <Switch>
              <Route path={urls.class} render={(props) => <StudentAssignmentsPage {...props} history={this.props.history} />} />
            </Switch> */}
             
                {/*<Switch>
                    <Route path={urls.class+ "/:id" + urls.assignment + "/:assignmentId"} render={(props) => <TeacherAssignments {...props} courseId={this.state.courseId} course={this.state.course}/>}/>
                    <Route path={urls.class+ "/:id" + urls.classPeople} render={(props) => <TeacherCoursePeople {...props} courseId={this.state.courseId} course={this.state.course}/>}/>
                    <Route path={urls.class+ "/:id"} render={(props) => <TeacherCourseAssignments {...props} courseId={this.state.courseId} course={this.state.course}/>}/>
          </Switch>*/}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
      courses: state.courses.courses,
      course: state.courses.course
  };
};

export default compose(
  connect(mapStateToProps, {getCourseById}),
    withTranslation(),
    withRouter
)(StudentCourseContainer);
