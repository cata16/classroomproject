import React, { Component } from "react";
import { combineReducers, compose } from "redux";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";
import Grid from "@material-ui/core/Grid";
import {getAssignmentDetails} from "../../actions/assignmentDetails"
import { useSSR } from "react-i18next";
import 'bootstrap/dist/css/bootstrap.min.css';  
// import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import FileListView from "../commons/widgets/FileListView";
import {Container , Card, Row, Col, Button} from 'react-bootstrap'; 

import "./AssignmentDetailsPage.css"
import UploadFile from "../commons/widgets/UploadFile";

class AssignmentDetailsPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            assignmentDetails: [],
            openUploadDialog: false,
            userId: null,
            assignmentId: null,
            files: []
        }
    }
      
    componentDidMount() {
		//window.localStorage.clear();
        const user = JSON.parse(localStorage.getItem('user'));
		var pathname = window.location.href.split("/");
        var assignmentId = pathname[pathname.length - 1];
        this.props
			.getAssignmentDetails(assignmentId, user.id
			)
			.then(() => {
                this.setState({
                    assignmentDetails: this.props.assignmentDetails,
                    userId: user.id,
                    assignmentId: assignmentId,
                    files: this.props.files,
                })
				//this.redirectToStudentPage();
				console.log("Get all assignment details");
			}).catch(() => {
				console.log("CatchComponentReject");
			});
        
	}

    componentDidUpdate(prevProps) {
        if(this.props.files != prevProps.files) {
            this.setState({files: this.props.files})
        }
    }

    handleUploadAssignment() {
        this.setState({openUploadDialog: true})
    }

    handleCloseUploadDialog = () => {
        this.setState({
            openUploadDialog: false,
        })
        window.location.reload();
    }

    renderUploadDialog = () => {
        return (
            <UploadFile
                //updatePercentage={(percentageData) => this.updatePercentage(percentageData)}
                open={this.state.openUploadDialog}
                handleClose={this.handleCloseUploadDialog} 
                assignmentId={this.state.assignmentId}
                userId={this.state.userId}/>
        )
    }

    renderFileList() {
        return <FileListView canDelete={true} files={this.state.files}/>
    }

    render (){ 
        return (    
            <div>
                <Container className="p-4">  
                <Row>
                {/* Assignment Title */}
                <h4> {this.state.assignmentDetails.title}</h4>
                <br></br>
                <br></br>
                <div></div>
                
                <Col>
                    <h6>Due date {this.state.assignmentDetails.dueDate}</h6>
                </Col>
                <Col>
                    <h6>Status {this.state.assignmentDetails.status}</h6>
                </Col>
                
                <hr />
                {/* Description */}
                <p class="description"> {this.state.assignmentDetails.description}</p>
                <div></div>
                </Row>
                
                <Row>
                <hr />
                </Row>
                <div></div>
                <Row>
                    <br></br><br></br>
                    <h6>Grade {this.state.assignmentDetails.grade} / 10</h6>
                    <br></br>
                    <h6>Feedback {this.state.assignmentDetails.feedback}</h6>
                    <br></br>
                </Row>
                <Button
                type="button" 
                className="btn-upload"
                onClick={() =>this.handleUploadAssignment()}
                >
                    Upload
                </Button>

                {this.renderUploadDialog()}
                {this.renderFileList()}
                </Container>
                
            </div>
        );
    }
}
const mapStateToProps = (state) => {
	return {
        assignmentDetails: state.assignmentDetails.assignmentDetails,
        files: state.assignmentDetails.assignmentFiles,
	};
};

export default compose(
    connect(mapStateToProps, {getAssignmentDetails}),
    withTranslation()
)(AssignmentDetailsPage);