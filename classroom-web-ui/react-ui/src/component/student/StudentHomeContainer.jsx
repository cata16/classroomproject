import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { Route, Switch } from "react-router";
import { urls } from "../../utils/utils";
import { withTranslation } from "react-i18next";
import Header from "./widgets/Header";
import StudentCoursesPage from "./StudentCoursesPage";

class StudentHomeContainer extends Component {
  render() {
    return (
      <div>
        <Header {...this.props} url={urls.student} />
        {/*<div>Student Home Page </div>*/}
        <div className="home-content">
          {/* <FilePagesSideBar />
                    <Divider orientation="vertical" flexItem /> */}
          <div className="home-data-container">
            {/*<StudentCoursesPage/>*/}
            {/*<Switch>
              <Route path={urls.student} component={StudentCoursesPage} />
                  </Switch> */}
            <Switch>
              <Route path={urls.student } render={(props) => <StudentCoursesPage {...props} history={this.props.history} />}/>
            </Switch> 
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {};
};

export default compose(
  connect(mapStateToProps, {}),
  withTranslation(),
)(StudentHomeContainer);
