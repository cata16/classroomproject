import React, { useState } from "react";
import { compose } from "redux";
import { withTranslation } from "react-i18next";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import { joinClass } from "../../../actions/courses";
import { urls } from "../../../utils/utils";

class JoinClassModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = { classCode: "" };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleJoinClass() {
    const user = JSON.parse(localStorage.getItem("user"));
    this.props.joinClass(this.state.classCode, user.id);
    window.location.reload();
  }

  handleChange(e) {
    this.setState({ classCode: e.target.value });
  }

  handleSubmit(event) {
    alert("You join the class!");
    event.preventDefault();
  }

  renderJoin() {
    return (
      <div>
        {/* <form onSubmit={this.handleSubmit}>
                    <label>
                    Code:
                    <input type="text" value={this.state.classCode} onChange={this.handleChange} />        </label>
                    <input type="submit" value="Submit" />
                </form> */}

        <p>Ask your teacher for the class code, then enter it here.</p>
        <input
          ref={(inputarea) => (this.inputArea = inputarea)}
          value={this.state.classCode}
          onChange={this.handleChange}
          placeholder="Class code"
        ></input>
      </div>
    );
  }

  render() {
    const { t } = this.props;
    return (
      <Dialog
        onClose={this.props.handleClose}
        open={this.props.open}
        disableEnforceFocus
        maxWidth="md"
      >
        <DialogTitle id="simple-dialog-title">
          <p>Class code</p>
        </DialogTitle>
        <DialogContent style={{ height: "20vh" }}>
          {this.renderJoin()}
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.handleClose} variant="outlined">
            {t("cancel")}
          </Button>
          <Button
            onClick={() => this.handleJoinClass()}
            color="primary"
            variant="contained"
          >
            {t("join")}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};
export default compose(
  connect(mapStateToProps, { joinClass }),
  withTranslation()
)(JoinClassModal);
