import { Component } from "react";
import logo from "../../../resources/logo.png";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { compose } from "redux";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";
import { logout } from "../../../actions/auth";
import { leaveClass } from "../../../actions/courses";
import LogoutIcon from "@mui/icons-material/Logout";
import Button from "@material-ui/core/Button";
import ListItemText from "@material-ui/core/ListItemText";
import { urls } from "../../../utils/utils";
import React, { useState } from "react";
import JoinClassModal from "./JoinClassModal";
import { withRouter } from "react-router";

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";

import "./Header.css";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openJoinClassModal: false,
      modalVisible: false,
      courseId: ''
    };
  }

  logout = () => {
    this.props.history.push(urls.login);
    this.props.logout();
  };

  redirectToProfile = () => {
    const user = JSON.parse(localStorage.getItem("user"));
    this.props.history.push(urls.profileStudent + "/" + user.id);
    window.location.reload();
  };

  redirectToClasswork = () => {
    this.props.history.push(urls.classStudent + "/" + this.props.courseId);
    window.location.reload();
  };

  redirectToCoursePeople = () => {
    this.props.history.push(urls.classStudent + "/" + this.props.courseId + urls.classPeople);
    window.location.reload();
  }

  redirectToLeave = () => {
    // window.location.reload();
    var proceed = window.confirm("Are you sure you want to proceed?");
    if (proceed) {
      //proceed 
      const user = JSON.parse(localStorage.getItem("user"));
      this.props.leaveClass(this.props.courseId, user.id);

      this.props.history.push(urls.student);
      window.location.reload();
    } 
    else {
      //don't proceed
    }
  };

  openModal = () => {
    this.setState({
      modalVisible: !this.state.modalVisible,
      openJoinClassModal: true,
    });
    //setShowModal(prev => !prev);
  };

  updateModalVisibility = (v) => {
    this.setState({ modalVisible: v });
  };

  handleCloseJoinClassModal = () => {
    this.setState({
      openJoinClassModal: false,
    });
  };

  renderCourseButtons() {
    const { t } = this.props;
    return (<>
              <Button
                variant="contained"
                color="primary"
                className="header-button"
                onClick={() => this.redirectToClasswork()}
              >
                {t("classwork")}
              </Button>
              <Button
                variant="contained"
                color="primary"
                className="header-button"
                onClick={() => this.redirectToCoursePeople()}
              >
                {t("people")}
              </Button>

              <Button
                variant="contained"
                color="primary"
                className="header-button"
                onClick={() => this.redirectToLeave()}
              >
                {t("leave")}
              </Button>
    </>);
  }

  renderHomeButtons() {
    const { t } = this.props;
    return(<>
              <Button
                variant="contained"
                color="primary"
                className="header-button"
                onClick={this.openModal}
              >
                {t("joinClass")}
              </Button>
              <Button
                variant="contained"
                color="primary"
                className="header-button"
                onClick={() => this.redirectToProfile()}
              >
                {t("profile")}
              </Button>
    </>)
  }

  renderButtons() {
    if(this.props.url == urls.classStudent) {
      return this.renderCourseButtons();
    }
    if(this.props.url == urls.student) {
      return this.renderHomeButtons();
    }
  }

  render() {
    const { t } = this.props;

    return (
      <div className="header-container">
        <AppBar position="static">
          <Toolbar className="header">
            <Link to={urls.login}>
              <img className="header-logo" src={logo} height="70" />
            </Link>
            <div className="header-buttons">
              {this.renderButtons()}
            </div>
              <JoinClassModal
                open={this.state.openJoinClassModal}
                handleClose={this.handleCloseJoinClassModal}
                on
              />
            <div className="header-content">
            <div className="header-action">
                <Button
                  variant="contained"
                  color="primary"
                  className="form-button"
                  onClick={() => this.logout()}>
                  {t("logout")}
                </Button> 
              </div>
            </div>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.auth.user,
  };
};
export default compose(
  connect(mapStateToProps, { logout, leaveClass }),
  withTranslation(),
  withRouter
)(Header);
