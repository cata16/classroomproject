import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";
import 'bootstrap/dist/css/bootstrap.min.css';  
import {Container ,Card,Row, Col, Button} from 'react-bootstrap';  
import { urls } from "../../../utils/utils";

import "./AssignmentWidget.css"

class AssignmentWidget extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    redirectToAssignmentDetails = () => {
        this.props.history.push(urls.classStudent + "/" + this.props.courseId + urls.assignment + "/" + this.props.assignment.id);
        window.location.reload();
    };

    render () {
        return (
            <Card className="assignment-widget" >
                <div>{this.props.assignment.title}</div>
                <br></br>
                <Button 
                type="button" 
                className="btn-view"
                onClick={() => this.redirectToAssignmentDetails()}
                >
                    View
                </Button>
            </Card>
        );
    }
}

const mapStateToProps = (state) => {
	return {
	};
};
export default compose(
	connect(mapStateToProps, {}),
	withTranslation()
)(AssignmentWidget);