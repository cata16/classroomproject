import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";
import 'bootstrap/dist/css/bootstrap.min.css';  
import {Container ,Card,Row, Col, Button} from 'react-bootstrap';  
import { urls } from "../../../utils/utils";

import "./ClassWidget.css"

class ClassWidget extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    redirectToAssignments = () => {
        this.props.history.push(urls.classStudent + "/" + this.props.course.id);
        window.location.reload();
    };

    render () {
        return (
            <Card className="class-widget" >
                <div>{this.props.course.name}</div>
                {/*<div>Teacher name {this.props.course.teacherId}</div>*/}
                <br></br>
                <Button 
                type="button" 
                className="btn-view"
                onClick={() => this.redirectToAssignments()}
                >
                    View
                </Button>
            </Card>
        );
    }
}

const mapStateToProps = (state) => {
	return {
	};
};
export default compose(
	connect(mapStateToProps, {}),
	withTranslation()
)(ClassWidget);