import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { Route, Switch } from "react-router";
import { urls } from "../../utils/utils";
import { withTranslation } from "react-i18next";
import Button from "@material-ui/core/Button";
import { getProfileDetails } from "../../actions/profileStudent";

import 'bootstrap/dist/css/bootstrap.min.css';
// import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import "./StudentProfilePage.css";

class StudentProfilePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      student: [],
      fullName: '',
      university: '',
      groupStudy: '',
      yearOfStudy: '',
      phoneNumber: '',
      email: '',
      oldPassword: '',
      newPassword: '',
      confirmNewPassword: '',
      oldPasswordRequired: false,
      newPasswordRequired: false,
      confirmNewPasswordRequired: false,
    };
    this.handleChangeFullName = this.handleChangeFullName.bind(this);
    this.handleChangeUniversity = this.handleChangeUniversity.bind(this);
    this.handleChangeGroupStudy = this.handleChangeGroupStudy.bind(this);
    this.handleChangeYearOfStudy = this.handleChangeYearOfStudy.bind(this);
    this.handleChangePhoneNumber = this.handleChangePhoneNumber.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
  }

  componentDidMount() {
    //window.localStorage.clear();
    const user = JSON.parse(localStorage.getItem("user"));
    this.props
      .getProfileDetails(user.id)
      .then(() => {
        this.setState({
          student: this.props.student,
        });
        //this.redirectToStudentPage();
        console.log("See profile details");
      })
      .catch(() => {
        console.log("CatchComponentReject");
      });
  }

  handleChangeFullName(e) {    
    this.setState({fullName: e.target.value});  
  }

  handleChangeUniversity(e) {    
    this.setState({university: e.target.value});  
  }

  handleChangeGroupStudy(e) {    
    this.setState({groupStudy: e.target.value});  
  }

  handleChangeYearOfStudy(e) {    
    this.setState({yearOfStudy: e.target.value});  
  }

  handleChangePhoneNumber(e) {    
    this.setState({phoneNumber: e.target.value});  
  }

  handleChangeEmail(e) {    
    this.setState({email: e.target.value});  
  }

  render() {
    return (
      <div>     
        <div class="container rounded bg-white mt-5 mb-5">
            <div class="row">
                <div class="col-md-5 border-right">
                    <div class="p-3 py-5">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <h4 class="text-right">Profile Settings</h4>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12"><label class="labels">Full Name</label><input type="text" class="form-control" placeholder={this.props.student.fullName} value={this.state.fullName}  onChange={this.handleChangeFullName}></input></div>
                            <div class="col-md-12"><label class="labels">University</label><input type="text" class="form-control" placeholder={this.props.student.university} value={this.state.university}  onChange={this.handleChangeUniversity}></input></div>
                            <div class="col-md-12"><label class="labels">Group Study</label><input type="text" class="form-control" placeholder={this.props.student.groupStudy} value={this.state.groupStudy}  onChange={this.handleChangeGroupStudy}></input></div>
                            <div class="col-md-12"><label class="labels">Year of Study</label><input type="text" class="form-control" placeholder={this.props.student.yearOfStudy} value={this.state.yearOfStudy}  onChange={this.handleChangeYearOfStudy}></input></div>
                            <div class="col-md-12"><label class="labels">Phone Number</label><input type="text" class="form-control" placeholder={this.props.student.phoneNumber} value={this.state.phoneNumber}  onChange={this.handleChangePhoneNumber}></input></div>
                            <div class="col-md-12"><label class="labels">Email Address</label><input type="text" class="form-control" placeholder={this.props.student.email} value={this.state.email}  onChange={this.handleChangeEmail}></input></div>
                        </div>
                        <div class="mt-5 text-center"><Button class="btn btn-primary profile-button">Change Request</Button></div>
                    </div>
                    <div class="p-3 py-5">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <h4 class="text-right">Change Password</h4>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12"><label class="labels">Old Password</label><input type="password" class="form-control" placeholder="" value={this.state.oldPassword} 
                            onChange={(e) =>
                              this.setState({
                                oldPassword: e.target.value,
                                oldPasswordRequired: false,
                              })  
									          }
                            ></input></div>
                            <div class="col-md-12"><label class="labels">New Password</label><input type="password" class="form-control" placeholder="" value={this.state.newPassword} 
                            onChange={(e) =>
                              this.setState({
                                newPassword: e.target.value,
                                newPasswordRequired: false,
                              })  
									          }
                            ></input></div>
                            <div class="col-md-12"><label class="labels">Confirm New Password</label><input type="password" class="form-control" placeholder="" value={this.state.confirmNewPassword} 
                            onChange={(e) =>
                              this.setState({
                                confirmNewPassword: e.target.value,
                                confirmNewPasswordRequired: false,
                              })  
									          }
                            ></input></div>
                        </div>
                        <div class="mt-5 text-center"><Button class="btn btn-primary profile-button">Change Password</Button></div>
                    </div>
                </div>
            </div>
        </div>
        {/* {<div> Email {this.props.student.email} </div> } */}

        
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { student: state.student.student };
};

export default compose(
  connect(mapStateToProps, { getProfileDetails }),
  withTranslation()
)(StudentProfilePage);
