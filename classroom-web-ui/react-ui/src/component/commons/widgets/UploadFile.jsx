import React, { useState } from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import { compose } from "redux";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";
// import {accessLevels} from "../../../actions/adminActions";
import { DropzoneDialog } from 'material-ui-dropzone';
import { submitFiles } from '../../../actions/assignments';

class UploadFile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    handleCancel = () => {
        this.props.handleClose();
    }

    handleCloseUploadFileModal = () => {
        this.setState({
            openUploadFilesModal: false,
        })
    }

    handleOpenUploadFileModal = () => {
        this.setState({
            openUploadFilesModal: true,
        })
    }

    handleSubmitFiles = (files, parentFileId) => {
        // this.props.updatePercentage({fileName:"file.name", procentageValue: "0"});
        this.props.submitFiles(files, this.props.userId, this.props.assignmentId, this.props.updatePercentage);
        this.props.handleClose();
    }

    render() {
        const { t } = this.props;
        return (
            <div>
                
                <DropzoneDialog
                    showPreviews={true}
                    showPreviewsInDropzone={false}
                    useChipsForPreview
                    filesLimit={10000}
                    inputProps={this.props.uploadFolder?{directory:"", webkitdirectory:"", type:"file"}: {}}
                    // inputProps={{directory:"", webkitdirectory:"", type:"file"}}
                    cancelButtonText={t('cancel')}
                    submitButtonText={t('submit')}
                    maxFileSize={30000000000}
                    previewGridProps={{ container: { spacing: 1, direction: 'row' } }}
                    previewText={t('selectedFiles')}
                    open={this.props.open}
                    onClose={this.props.handleClose}
                    onSave={(files) => this.handleSubmitFiles(files)}
                />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
    }
}
export default compose(
    connect(mapStateToProps, { submitFiles }),
    withTranslation())(UploadFile);