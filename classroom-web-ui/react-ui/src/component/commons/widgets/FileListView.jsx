import React, { Component } from "react";
import { combineReducers, compose } from "redux";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";
import FileView from "./FileView";

class FileListView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            files:[]
        }
    }

    componentDidMount() {
        this.setState({files: this.props.files});
    }

    componentDidUpdate(prevProps) {
        if(this.props.files != prevProps.files) {
            this.setState({files: this.props.files});
        }
    }

    renderFiles() {
        return(this.state.files.map(file => <FileView canDelete={this.props.canDelete} file={file}/>));
    }


    render() {
        const { t } = this.props;
        return (
            <div>
                {this.renderFiles()}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
    }
}
export default compose(
    connect(mapStateToProps, {  }),
withTranslation())(FileListView);