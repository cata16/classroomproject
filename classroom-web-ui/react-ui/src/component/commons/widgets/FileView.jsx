import React, { Component } from "react";
import { combineReducers, compose } from "redux";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";
import FileDownloadIcon from '@mui/icons-material/FileDownload';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import {downloadFile, deleteFile} from '../../../actions/assignments';

class FileView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }

    render() {
        const { t } = this.props;
        return (
            <div style={{"display":"inline"}}>
                <div>
                    {this.props.file.name + "." + this.props.file.extension}
                </div>
                <FileDownloadIcon onClick={() => this.props.downloadFile(this.props.file)}/>
                {this.props.canDelete?<DeleteForeverIcon onClick={() => this.props.deleteFile(this.props.file)}/>: null}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
    }
}
export default compose(
    connect(mapStateToProps, { downloadFile, deleteFile }),
    withTranslation())(FileView);