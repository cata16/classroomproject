import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { login } from "../../actions/auth";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import SupervisedUserCircleSharp from "@material-ui/icons/SupervisedUserCircleSharp";
import LockOpenSharp from "@material-ui/icons/LockOpenSharp";
import { withTranslation } from "react-i18next";
import {Link} from 'react-router-dom';
import logo from "../../resources/logo.png";
import {urls} from "../../utils/utils";

import "./LoginContainer.css";
const styles = (theme) => ({
	textField: {
		backgroundColor: "#987fbd",
	},
});

class LoginContainer extends Component {
	state = {
		email: "",
		password: "",
		emailRequired: false,
		passwordRequired: false,
	};

	componentWillMount() {
		//window.localStorage.clear();
		if (this.props.isLoggedIn) {
			this.redirectHomePage();
		}
	}

	redirectHomePage() {
		//if(this.props.role == "Student") {
			this.redirectToTeacherPage();
		//}
	}

	redirectToTeacherPage() {
		this.props.history.push(urls.teacher);
		window.location.reload();
	}

	redirectToStudentPage() {
		this.props.history.push('/');
		window.location.reload();
	}

	onEnterKey(e) {
		if (e.which === 13) {
			this.onFormSubmit();
		}
	}

	onFormSubmit = () => {
		if (this.areValidInputs() === false) {
			return;
		}
		this.props
			.login(
				this.state.email,
				this.state.password,
			)
			.then(() => {
				this.redirectToStudentPage();
				console.log("ComponentResolve");
			}).catch(() => {
				console.log("CatchComponentReject");
			});
	};

	areValidInputs = () => {
		let validInputs = true;

		if (this.checkEmail() == false) {
			validInputs = false;
		}
		if (this.checkPassword() == false) {
			validInputs = false;
		}

		return validInputs;
	};

	checkEmail = () => {
		if (this.state.email === "") {
			this.setState({
				emailRequired: true,
			});
			return false;
		}
		this.setState({
			emailRequired: false,
		});
		return true;
	};

	checkPassword = () => {
		if (this.state.password === "") {
			this.setState({
				passwordRequired: true,
			});
			return false;
		}
		this.setState({
			passwordRequired: false,
		});
		return true;
	};

	render() {
		const { t } = this.props;
		return (
			<div className="login-content">
				<Paper className="">
					{/* <div>{this.renderLoginFailed()}</div> */}
					<div className="login-wrapper">
						<form className="login-wrapper__form">
							<FormControl
								margin="normal"
								required
								fullWidth
							>
								<center style={{ marginBottom: "20px" }}>
									<img src={logo} width="400" height="200" />
								</center>
								<TextField
									id="email"
									value={this.state.email}
									onChange={(e) =>
										this.setState({
											email: e.target.value,
											emailRequired: false,
										})
									}
									onKeyPress={(e) => this.onEnterKey(e)}
									type="text"
									label={t("email")}
									variant="outlined"
									margin="normal"
									className={styles.textField}
									required
									error={this.state.emailRequired}
									InputProps={{
										startAdornment: (
											<InputAdornment position="start">
												<SupervisedUserCircleSharp />
											</InputAdornment>
										),
									}}
								/>
							</FormControl>
							<FormControl margin="normal" required fullWidth>
								<TextField
									id="password"
									value={this.state.password}
									onChange={(e) =>
										this.setState({
											password: e.target.value,
											passwordRequired: false,
										})
									}
									onKeyPress={(e) => this.onEnterKey(e)}
									type="password"
									label={t("password")}
									margin="normal"
									variant="outlined"
									required
									error={this.state.passwordRequired}
									InputProps={{
										startAdornment: (
											<InputAdornment position="start">
												<LockOpenSharp />
											</InputAdornment>
										),
									}}
								/>
							</FormControl>
							<Button
								variant="contained"
								color="primary"
								className="form-button"
								onClick={() => this.onFormSubmit()}
							>
								{t("login")}
							</Button>
							<span className="link">
								<Link className="link" to="/reset-password" >{t("forgotPassword")}</Link>
							</span>
							{/* <a href="#" className="sidebar-item" onClick={(e) => this.handleOpenResetPassword()} style={{ alignSelf: "flex-end", marginRight: "20px !important", color: "#FFF" }}>
								{t("forgotPassword")}</a> */}
							{/* <ResetPassword
                                open={this.state.openResetPassword}
                                handleClose={(e) => this.handleCloseResetPassword()}
                            /> */}
							{/*
                            <a href="#" class="sidebar-item" onClick={(e) => this.handleOpenCreateAccount()} style={{ alignSelf: "flex-end", marginRight: "20px !important", color: "#FFF" }}>
                                Don't have an account?</a>
                            <CreateAccount
                                open={this.state.openCreateAccount}
                                handleClose={(e) => this.handleCloseCreateAccount()}
                            /> */}
						</form>
					</div>
					<footer className="login-footer">{t("slogan")}</footer>
				</Paper>
			</div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		// loginFailed: state.adminReducer.loginError,
		isLoggedIn: state.auth.isLoggedIn,
		role: state.auth.role
	};
};
export default compose(
	connect(mapStateToProps, {login}),
	withTranslation()
)(LoginContainer);
