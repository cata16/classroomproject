import {
    GET_ALL_ASSIGNMENTS_COURSE, CREATE_ASSIGNMENT, GET_ASSIGNMENT_BY_ID
} from "../actions/types";
  

const initialState = {assignments: [], currentAssignment: {}};

export default function (state = initialState, action) {
    const {type, payload} = action;
    
    switch(type) {
        case GET_ALL_ASSIGNMENTS_COURSE: 
            return {
                ...state,
                assignments: payload,
            };
        case CREATE_ASSIGNMENT: 
            return {
                ...state,
                assignments: payload,
            };
        case GET_ASSIGNMENT_BY_ID:
            return {
                ...state,
                currentAssignment: payload
            }
        default:
            console.log(state);
            return state;
    }
}