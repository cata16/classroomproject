import { combineReducers } from "redux";
import auth from "./auth";
import courses from "./courses";
import student from "./profileStudent";
import assignments from "./assignments";
import assignmentDetails from "./assignmentDetails";
import teacher from "./profileTeacher";

export default combineReducers({
  auth,
  courses: courses,
  student: student,
  assignments: assignments,
  assignmentDetails: assignmentDetails,
  teacher: teacher,
});
