import {
    LOGIN_SUCCESS,
    LOGOUT
  } from "../actions/types";
  
const user = JSON.parse(localStorage.getItem("user"));

const initialState = user
  ? { isLoggedIn: true, roles:user.roles, user }
  : { isLoggedIn: false, roles:[], user: null };

export default function (state = initialState, action) {
    const {type, payload} = action;
    
    switch(type) {
        case LOGIN_SUCCESS: 
            localStorage.setItem("user", JSON.stringify(payload));
            return {
                ...state,
                isLoggedIn: true,
                user: payload,
            };
        case LOGOUT:
            localStorage.removeItem("user");
            return {
                ...state,
                isLoggedIn: false,
                user: null
            }
        default:
            console.log(state);
            return state;
    }
}