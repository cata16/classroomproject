import { GET_PROFILE_DETAILS_TEACHER } from "../actions/types";

const initialState = {teacher: []};


export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_PROFILE_DETAILS_TEACHER:
      return {
        ...state,
        teacher: payload,
      };
    default:
      console.log(state);
      return state;
  }
}
