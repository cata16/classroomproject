import {
    GET_ALL_COURSES,
    JOIN_CLASS,
    CREATE_CLASS,
    GET_ALL_COURSES_STUDENT,
    GET_ALL_COURSE_STUDENTS,
    GET_COURSE_BY_ID,
    LEAVE_CLASS
} from "../actions/types";
  

const initialState = {courses: [], students: [], course: null};

export default function (state = initialState, action) {
    const {type, payload} = action;
    
    switch(type) {
        case GET_ALL_COURSES: 
            return {
                ...state,
                courses: payload,
            };
        case JOIN_CLASS: 
            return {
                ...state,
                courses: payload,
            };
        case CREATE_CLASS: 
            return {
                ...state,
                courses:  [...state.courses, payload],
            };
        case GET_ALL_COURSES:
            return {
                ...state,
                courses: payload,
            };
        case GET_ALL_COURSE_STUDENTS: 
            return {
                ...state,
                students: payload
            }
        case GET_COURSE_BY_ID:
            return {
                ...state,
                course: payload
            }
        case LEAVE_CLASS:
            return {
                ...state,
                course: payload
            }
        default:
            console.log(state);
            return state;
    }
}