import {
    GET_ASSIGNMENT_DETAILS,
    FILES_UPLOADED,
    DELETED_FILE
} from "../actions/types";
  

const initialState = {assignmentDetails: null, assignmentFiles: []};

export default function (state = initialState, action) {
    const {type, payload} = action;
    
    switch(type) {
        case GET_ASSIGNMENT_DETAILS: 
            return {
                ...state,
                assignmentDetails: payload,
                assignmentFiles: payload.files
            };
        case FILES_UPLOADED:
            return {
                ...state,
                assignmentFiles: [...state.assignmentFiles, ...payload]
            };
        case DELETED_FILE:
            let newFileList = state.assignmentFiles.filter((file) => {
                return file.id !== action.payload.id ? file : null
            });
            return {
                ...state,
                assignmentFiles: newFileList
            }
        default:
            console.log(state);
            return state;
    }
}