import { GET_PROFILE_DETAILS_STUDENT } from "../actions/types";

const initialState = {student: []};


export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_PROFILE_DETAILS_STUDENT:
      return {
        ...state,
        student: payload,
      };
    default:
      console.log(state);
      return state;
  }
}
