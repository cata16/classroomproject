import React from "react"
import {
    Redirect,
    Route,
} from 'react-router-dom';
import { urls } from "../utils/utils";

const PrivateRoute = ({ component: Component, authed, userRoles, accessRoles, ...rest }) => {
    return (
        <Route
            {...rest}
            render={(props) => authed === true && (!accessRoles || accessRoles.some(role => (userRoles.includes(role))))
                ? <Component {...props} />
                : 
                // <Component {...props} />}
                <Redirect to={{ pathname: urls.login, }} />}
        />
    )
}

export default PrivateRoute;