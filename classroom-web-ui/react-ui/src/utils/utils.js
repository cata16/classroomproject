
import Alert from 'react-s-alert';

export const currentUrl = window.location.hostname;
export const serverPort = 8090
export const serverProtocol = "http"
export const basePath = '/api'

export const urls ={
    login:'/login',
    student: '/',
    teacher: '/',
    joinClass: '/joinClass',
    profileStudent: '/profileStudent',
    class: '/class',
    classPeople: '/people',
    assignmentDetails: '/assignmentDetails',
    people: '/course/people',
    assignment: '/assignment',
    profileTeacher: '/profileTeacher',
    classStudent: '/classStudent'
}
