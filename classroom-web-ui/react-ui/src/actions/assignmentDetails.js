import {GET_ASSIGNMENT_DETAILS, ASSIGNMENT_GRADED} from './types';
import AssignmentDetailsService from '../services/assignmentDetailsService';

export const getAssignmentDetails = (assignmentId, studentId) => (dispatch) => {
return AssignmentDetailsService.getAssignmentDetails(assignmentId, studentId).then(
    (response) => {
        if(response.status==200) {
            console.log(response);
            dispatch ({
                type: GET_ASSIGNMENT_DETAILS,
                payload: response.data
            })
        }else {
            console.log("noResponse");
        }
        return Promise.resolve();
    },
    (error) => {
        console.log(error);
        return Promise.reject();
    }
)
}

export const gradeAssignment = (studentAssignmentId, grade) => dispatch => {
    return AssignmentDetailsService.gradeAssignment(studentAssignmentId, grade).then(
        (response) => {
            if(response.status==200) {
                console.log(response);
                dispatch ({
                    type: ASSIGNMENT_GRADED,
                    payload: response.data
                })
            }else {
                console.log("noResponse");
            }
            return Promise.resolve();
        },
        (error) => {
            console.log(error);
            return Promise.reject();
        }
    )
}