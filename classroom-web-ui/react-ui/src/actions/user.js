import UserService from '../services/userService';
import {USER_CREATED, 
        USER_UPDATED,
        FETCH_USERS,
        PERSONAL_INFO
} from './types';

export const createUser = (newUser) => (dispatch) => {
    return UserService.create(newUser).then(
        (response) => {
            if(response.status==201) {
                dispatch ({
                    type: USER_CREATED,
                    payload: response.data
                })
            }else {
                // console.log("noResponse");
            }
            return Promise.resolve();
        },
        (error) => {
            if(error.status == 403){
                console.log(error);
            }
            return Promise.reject();
        }
    )
}

export const updateUser = (userToUpdate) => (dispatch) => {
    return UserService.update(userToUpdate).then(
        (response) => {
            if(response.status==200) {
                dispatch ({
                    type: USER_UPDATED,
                    payload: response.data
                })
                return Promise.resolve();
            }else {
            }
            return Promise.reject();
        },
        (error) => {
            if(error.status == 403){
                console.log(error);
            }
            return Promise.reject();
        }
    )
}

export const fetchAllUsers = () => (dispatch) => {
    return UserService.fetchAllUsers().then(
        (response) => {
            if(response.status==200) {
                dispatch ({
                    type: FETCH_USERS,
                    payload: response.data
                })
            }
            return Promise.resolve();
        },
        (error) => {
            if(error.status == 401){
                console.log(error);
            }
            return Promise.reject();
        }
    ) 
}

export const getPersonalInformations = () => (dispatch) => {
    return UserService.getPersonalInforamtions().then(
        (response) => {
            if(response.status==200) {
                dispatch ({
                    type: PERSONAL_INFO,
                    payload: response.data
                })
                return Promise.resolve();
            } else {
                dispatch ({
                    type: PERSONAL_INFO,
                    payload: null
                })
                return Promise.reject();
            }
        },
        (error) => {
            if(error.status == 401){
                console.log(error);
            }
            dispatch ({
                type: PERSONAL_INFO,
                payload: null
            })
            return Promise.reject();
        }
    )  
}

export const changePassword = (passwords) => (dispatch) => {
    return UserService.changePassword(passwords).then(
        (response) => {
            if(response.status==200) {
               
                return Promise.resolve();
            }
            return Promise.reject();
        },
        (error) => {
            if(error.status == 401){
                console.log(error);
            }
            return Promise.reject();
        }
    ) 
}