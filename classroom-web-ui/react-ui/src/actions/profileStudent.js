import { GET_PROFILE_DETAILS_STUDENT } from "./types";
import StudentService from "../services/profileStudentService";

export const getProfileDetails = (studentId) => (dispatch) => {
  return StudentService.getProfileDetails(studentId).then(
    (response) => {
      if (response.status == 200) {
        console.log(response);
        dispatch({
          type: GET_PROFILE_DETAILS_STUDENT,
          payload: response.data,
        });
      } else {
        console.log("noResponse");
      }
      return Promise.resolve();
    },
    (error) => {
      console.log(error);
      return Promise.reject();
    }
  );
};
