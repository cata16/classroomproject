import {GET_ALL_COURSES, GET_ALL_COURSES_STUDENT, JOIN_CLASS, CREATE_CLASS, GET_ALL_COURSE_STUDENTS, GET_COURSE_BY_ID,
    LEAVE_CLASS} from './types';
import CoursesService from '../services/coursesService';

export const getAllCourses = () => (dispatch) => {
return CoursesService.getAllCourses().then(
    (response) => {
        if(response.status==200) {
            console.log(response);
            dispatch ({
                type: GET_ALL_COURSES,
                payload: response.data
            })
        }else {
            console.log("noResponse");
        }
        return Promise.resolve();
    },
    (error) => {
        console.log(error);
        return Promise.reject();
    }
)
}

export const joinClass = (classCode, studentId) => (dispatch) => {
    return CoursesService.joinClass(classCode, studentId).then(
        (response) => {
            if(response.status==200) {
                console.log(response);
                dispatch ({
                    type: JOIN_CLASS,
                    payload: response.data
                })
            }else {
                console.log("noResponse");
            }
            return Promise.resolve();
        },
        (error) => {
            console.log(error);
            return Promise.reject();
        }
    )
}

export const createClass = (className, teacherId) => (dispatch) => {
    return CoursesService.createClass(className, teacherId).then(
        (response) => {
            if(response.status==201) {
                console.log(response);
                dispatch ({
                    type: CREATE_CLASS,
                    payload: response.data
                })
            }else {
                console.log("noResponse");
            }
            return Promise.resolve();
        },
        (error) => {
            console.log(error);
            return Promise.reject();
        }
    )
}

export const getAllCoursesStudent = (studentId) => (dispatch) => {
    return CoursesService.getAllCoursesStudent(studentId).then(
        (response) => {
            if(response.status==200) {
                console.log(response);
                dispatch ({
                    type: GET_ALL_COURSES,
                    payload: response.data
                })
            }else {
                console.log("noResponse");
            }
            return Promise.resolve();
        },
        (error) => {
            console.log(error);
            return Promise.reject();
        }
    )
}

export const getAllCoursesTeacher = (teacherId) => (dispatch) => {
    return CoursesService.getAllCoursesTeacher(teacherId).then(
        (response) => {
            if(response.status==200) {
                console.log(response);
                dispatch ({
                    type: GET_ALL_COURSES,
                    payload: response.data
                })
            }else {
                console.log("noResponse");
            }
            return Promise.resolve();
        },
        (error) => {
            console.log(error);
            return Promise.reject();
        }
    )
}

export const getAllCoursePeople = (courseId) => (dispatch) => {
    return CoursesService.getAllCoursePeople(courseId).then(
        (response) => {
            if(response.status==200) {
                dispatch ({
                    type: GET_ALL_COURSE_STUDENTS,
                    payload: response.data
                })
            }else {
                console.log("noResponse");
            }
            return Promise.resolve();
        },
        (error) => {
            console.log(error);
            return Promise.reject();
        }
    )
}

export const getCourseById = (courseId) => (dispatch) => {
    return CoursesService.getCourseById(courseId).then(
        (response) => {
            if(response.status==200) {
                dispatch ({
                    type: GET_COURSE_BY_ID,
                    payload: response.data
                })
            }else {
                console.log("noResponse");
            }
            return Promise.resolve();
        },
        (error) => {
            console.log(error);
            return Promise.reject();
        }
    ) 
}

export const leaveClass = (courseId, studentId) => (dispatch) => {
    return CoursesService.leaveClass(courseId, studentId).then(
        (response) => {
            if(response.status==200) {
                dispatch ({
                    type: LEAVE_CLASS,
                    payload: response.data
                })
            }else {
                console.log("noResponse");
            }
            return Promise.resolve();
        },
        (error) => {
            console.log(error);
            return Promise.reject();
        }
    ) 
}