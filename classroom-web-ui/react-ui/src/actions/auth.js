import {LOGIN_SUCCESS, 
        LOGOUT} from './types';
import AuthService from '../services/authService';

export const login = (email, password) => (dispatch) => {
    return AuthService.login(email, password).then(
        (response) => {
            if(response.status==200) {
                console.log(response);
                dispatch ({
                    type: LOGIN_SUCCESS,
                    payload: response.data
                })
            }else {
                console.log("noResponse");
            }
            return Promise.resolve();
        },
        (error) => {
            console.log(error);
            return Promise.reject();
        }
    )
}

export const logout = () => (dispatch) => {
    dispatch({
        type:LOGOUT
    })
}

export const resetPassword = (email) => (dispatch) => {
    return AuthService.resetPassword(email).then(
        (response) => {
            if(response.status==200) {
                return Promise.resolve();
            }
        },
        (error) => {
            return Promise.reject();
        }
    )
}