import {GET_ALL_ASSIGNMENTS_COURSE, FILES_UPLOADED, CREATE_ASSIGNMENT, DELETED_FILE, GET_ASSIGNMENT_BY_ID} from './types';
import AssignmentsService from '../services/assignmentsService';
import { WritableStream } from 'web-streams-polyfill/ponyfill';
import streamSaver from 'streamsaver';

export const getAllAssignmentsCourse = (courseId) => (dispatch) => {
return AssignmentsService.getAllAssignmentsCourse(courseId).then(
    (response) => {
        if(response.status==200) {
            console.log(response);
            dispatch ({
                type: GET_ALL_ASSIGNMENTS_COURSE,
                payload: response.data
            })
        }else {
            console.log("noResponse");
        }
        return Promise.resolve();
    },
    (error) => {
        console.log(error);
        return Promise.reject();
    }
)
}

export const submitFiles = (files, userId, assignmentId) => (dispatch) => {
    console.log(userId +"/"+assignmentId);
    return AssignmentsService.submitFiles(files, userId, assignmentId)
    .then(
        (responses) => {
            let addedFiles = responses.filter((response) => response.status == 200 ? response: null)
                                    .map((response) => response.data);
            addedFiles = addedFiles.filter((v,i,a)=>a.findIndex(t=>(t.id === v.id))===i)
            dispatch({
                type: FILES_UPLOADED,
                payload: addedFiles
            })
            return Promise.resolve();
        },
        (error) => {
            //updatePercentage(null);
            // if(error.status === 409) {
            //     errorNotification("File already Exists");
            // }
            // if(error.status == 401){
            //     console.log(error);
            // }
            return Promise.reject();
        }
    ) 
}

export const deleteFile = (file) => (dispatch) => {
    AssignmentsService.delete(file)
        .then((response) => {
            if(response.status == 200) {
                dispatch({
                    type: DELETED_FILE,
                    payload: response.data,
                })
            }
            return Promise.resolve();
        },
        (error) => {
            return Promise.reject();
        })
}

export const downloadFile = (file) => (dispatch) => {
    AssignmentsService.download(file)
    .then((response) => {
        handleDownloadFromResponse(response);        
        
        return Promise.resolve();
    },
    (error) => {
        return Promise.reject();
    })
}

const handleDownloadFromResponse = (response) => {
    let contentDisposition = response.headers.get('Content-Disposition');
        let fileName = contentDisposition.substring(contentDisposition.lastIndexOf('=') + 1);
    
        // These code section is adapted from an example of the StreamSaver.js
        // https://jimmywarting.github.io/StreamSaver.js/examples/fetch.html
    
        // If the WritableStream is not available (Firefox, Safari), take it from the ponyfill
        if (!window.WritableStream) {
            streamSaver.WritableStream = WritableStream;
            window.WritableStream = WritableStream;
        }
    
        const fileStream = streamSaver.createWriteStream(fileName);
        const readableStream = response.body;
    
        // More optimized
        if (readableStream.pipeTo) {
            return readableStream.pipeTo(fileStream);
        }
    
        const writer = fileStream.getWriter();
    
        const reader = response.body.getReader();
        const pump = () => reader.read()
            .then(res => res.done
                ? writer.close()
                : writer.write(res.value).then(pump));
    
        pump();
}

export const createAssignment = (title, description, dueDate, courseId) => (dispatch) => {
    return AssignmentsService.createAssignment(title, description, dueDate, courseId).then(
        (response) => {
            if(response.status==200) {
                console.log(response);
                dispatch ({
                    type: CREATE_ASSIGNMENT,
                    payload: response.data
                })
            }else {
                console.log("noResponse");
            }
            return Promise.resolve();
        },
        (error) => {
            console.log(error);
            return Promise.reject();
        }
    )
}

export const getAssignmentById = (assignmentId) => (dispatch) => {
    return  AssignmentsService.getAssignmentById(assignmentId).then(
        (response) => {
            if(response.status==200) {
                console.log(response);
                dispatch ({
                    type: GET_ASSIGNMENT_BY_ID,
                    payload: response.data
                })
            }else {
                console.log("noResponse");
            }
            return Promise.resolve();
        },
        (error) => {
            console.log(error);
            return Promise.reject();
        }
    )
}