import { GET_PROFILE_DETAILS_TEACHER } from "./types";
import TeacherService from "../services/profileTeacherService";

export const getProfileDetails = (teacherId) => (dispatch) => {
  return TeacherService.getProfileDetails(teacherId).then(
    (response) => {
      if (response.status == 200) {
        console.log(response);
        dispatch({
          type: GET_PROFILE_DETAILS_TEACHER,
          payload: response.data,
        });
      } else {
        console.log("noResponse");
      }
      return Promise.resolve();
    },
    (error) => {
      console.log(error);
      return Promise.reject();
    }
  );
};
