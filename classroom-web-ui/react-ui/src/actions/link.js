import LinkService from '../services/linkService';
import {
    FETCH_FILES,
    FETCH_LINKS,
    DELETED_LINK,
    CREATED_LINK
} from './types';

export const getLinkFiles = (linkId, parentId) => (dispatch) => {
    return LinkService.getLinkFiles(linkId, parentId).then(
        (response) => {
            if(response.status==200) {
                dispatch({
                    type: FETCH_FILES,
                    payload: response.data
                });
            }else {
                // console.log("noResponse");
            }
            return Promise.resolve();
        },
        (error) => {
            // if(error.status == 403){
            //     console.log(error);
            // }
            return Promise.reject();
        }
    )
}

export const getLinksForFile = (selectedFile) => (dispatch) => {
    return LinkService.getLinksForFile(selectedFile).then(
        (response) => {
            if(response.status==200) {
                dispatch({
                    type: FETCH_LINKS,
                    payload: response.data
                });
            }else {
                // console.log("noResponse");
            }
            return Promise.resolve();
        },
        (error) => {
            // if(error.status == 403){
            //     console.log(error);
            // }
            return Promise.reject();
        }
    )
}

export const deleteLink = (link) => (dispatch) => {
    return LinkService.deleteLink(link).then(
        (response) => {
            if(response.status==200) {
                dispatch({
                    type: DELETED_LINK,
                    payload: response.data
                });
            }else {
                // console.log("noResponse");
            }
            return Promise.resolve();
        },
        (error) => {
            // if(error.status == 403){
            //     console.log(error);
            // }
            return Promise.reject();
        }
    )
}

export const createLink = (file) => (dispatch) => {
    return LinkService.createLink(file).then(
        (response) => {
            if(response.status==200) {
                dispatch({
                    type: CREATED_LINK,
                    payload: response.data
                });
            }else {
                // console.log("noResponse");
            }
            return Promise.resolve();
        },
        (error) => {
            // if(error.status == 403){
            //     console.log(error);
            // }
            return Promise.reject();
        }
    )
}