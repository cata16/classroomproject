import FileService from '../services/fileService';
import {
    FETCH_FILES,
    FETCH_FOLDER_STRUCTURE,
    DELETED_FILE,
    FILE_CREATED,
    FILES_UPLOADED,
    DELETED_FILES,
    REMOVED_FROM_FAVORITES,
    ADDED_TO_FAVORITES,
    FETCH_FAVORITES,
} from './types';

import { WritableStream } from 'web-streams-polyfill/ponyfill';
import streamSaver from 'streamsaver';
import { errorNotification } from '../utils/utils';

export const submitFiles = (files, parentId, updatePercentage) => (dispatch) => {
    FileService.submitFiles(files, parentId, updatePercentage)
    .then(
        (responses) => {
            let addedFiles = responses.filter((response) => response.status == 200 ? response: null)
                                    .map((response) => response.data);
            addedFiles = addedFiles.filter((v,i,a)=>a.findIndex(t=>(t.id === v.id))===i)
            dispatch({
                type: FILES_UPLOADED,
                payload: addedFiles
            })
            return Promise.resolve();
        },
        (error) => {
            updatePercentage(null);
            if(error.status === 409) {
                errorNotification("File already Exists");
            }
            // if(error.status == 401){
            //     console.log(error);
            // }
            return Promise.reject();
        }
    ) 
}

export const getFilesByParentId = (parentFileId) => (dispatch) => {
    return FileService.getByParentId(parentFileId)
        .then((response) => {
            if(response.status == 200) {
                dispatch({
                    type: FETCH_FILES,
                    payload: response.data
                });
            }
            return Promise.resolve();
        }, 
        (error) => {
            //TODO: add error handling
            return Promise.reject();
        })
}

export const getFolderStructureByName = (path) => (dispatch) => {
    if(!path || path == "") {
        dispatch({
            type: FETCH_FOLDER_STRUCTURE,
            payload: []
        });
        return Promise.resolve();
    }
    return FileService.getFolderHierarchyByPath(path)
    .then((response) => {
        if(response.status == 200) {
            dispatch({
                type: FETCH_FOLDER_STRUCTURE,
                payload: response.data
            });
        }
        return Promise.resolve();
    }, 
    (error) => {
        //TODO: add error handling
        return Promise.reject();
    })
    return Promise.resolve;
}

export const createFolder = (folderName, currentFileId) => (dispatch) => {
    return FileService.createFolder(folderName, currentFileId)
        .then((response) => {
            if(response.status == 200) {
                dispatch({
                    type:FILE_CREATED,
                    payload: response.data,
                })
            }
            return Promise.resolve();
        },
        (error) => {
            return Promise.reject();
        })
}

export const downloadFile = (file) => (dispatch) => {
    FileService.test(file)
    .then((response) => {
        handleDownloadFromResponse(response);        
        
        return Promise.resolve();
    },
    (error) => {
        return Promise.reject();
    })
}

export const downloadFileFromLink = (file, linkId) => (dispatch) => {
    FileService.downloadFileFromLink(file, linkId)
    .then((response) => {
        handleDownloadFromResponse(response);        
        
        return Promise.resolve();
    },
    (error) => {
        return Promise.reject();
    })
}

export const deleteFile = (file) => (dispatch) => {
    FileService.deleteFile(file)
        .then((response) => {
            if(response.status == 200) {
                dispatch({
                    type: DELETED_FILE,
                    payload: response.data,
                })
            }
            return Promise.resolve();
        },
        (error) => {
            return Promise.reject();
        })
}

export const downloadMultipleFiles = (files) => (dispatch) => {

    FileService.downloadFiles(files)
    .then((response) => {
        handleDownloadFromResponse(response);        
        return Promise.resolve();
    },
    (error) => {
        return Promise.reject();
    })
}

export const downloadMultipleFilesFromLink = (files, linkId) => (dispatch) => {
    FileService.downloadFilesFromLink(files, linkId)
    .then((response) => {
        handleDownloadFromResponse(response);        
        
        return Promise.resolve();
    },
    (error) => {
        return Promise.reject();
    })
}
export const deleteMultipleFiles = (files) => (dispatch) => {
    FileService.deleteFiles(files)
    .then(
        (responses) => {
            let deletedFiles = responses.filter((response) => response.status == 'fulfilled' && response.value.status ? response: null)
                                    .map((response) => response.value.data);

            dispatch({
                type: DELETED_FILES,
                payload: deletedFiles
            })
            return Promise.resolve();
        },
        (error) => {
            return Promise.reject();
        }
    ) 
}

export const addToFavorites = (file) => (dispatch) => {
    FileService.addToFavorites(file)
    .then((response) => {
        if(response.status == 200) {
            dispatch({
                type: ADDED_TO_FAVORITES,
                payload: response.data,
            })
        }
        return Promise.resolve();
    },
    (error) => {
        return Promise.reject();
    })
} 

export const removeFromFavorites = (file) => (dispatch) => {
    FileService.removeFromFavorites(file)
    .then((response) => {
        if(response.status == 200) {
            dispatch({
                type: REMOVED_FROM_FAVORITES,
                payload: response.data,
            })
        }
        return Promise.resolve();
    },
    (error) => {
        return Promise.reject();
    })
}

export const getFavoriteFiles = () => (dispatch) => {
    FileService.getFavoriteFiles()
    .then((response) => {
        if(response.status == 200) {
            dispatch({
                type: FETCH_FAVORITES,
                payload: response.data,
            })
        }
        return Promise.resolve();
    },
    (error) => {
        return Promise.reject();
    })
}

const handleDownloadFromResponse = (response) => {
    let contentDisposition = response.headers.get('Content-Disposition');
        let fileName = contentDisposition.substring(contentDisposition.lastIndexOf('=') + 1);
    
        // These code section is adapted from an example of the StreamSaver.js
        // https://jimmywarting.github.io/StreamSaver.js/examples/fetch.html
    
        // If the WritableStream is not available (Firefox, Safari), take it from the ponyfill
        if (!window.WritableStream) {
            streamSaver.WritableStream = WritableStream;
            window.WritableStream = WritableStream;
        }
    
        const fileStream = streamSaver.createWriteStream(fileName);
        const readableStream = response.body;
    
        // More optimized
        if (readableStream.pipeTo) {
            return readableStream.pipeTo(fileStream);
        }
    
        const writer = fileStream.getWriter();
    
        const reader = response.body.getReader();
        const pump = () => reader.read()
            .then(res => res.done
                ? writer.close()
                : writer.write(res.value).then(pump));
    
        pump();
}
