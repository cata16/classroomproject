import axios from 'axios';

import {currentUrl, serverPort, serverProtocol, basePath} from '../utils/utils';

export default axios.create({
    baseURL: `${serverProtocol}://${currentUrl}:${serverPort}${basePath}/auth`
})