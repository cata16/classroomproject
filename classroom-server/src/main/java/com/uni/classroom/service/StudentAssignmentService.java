package com.uni.classroom.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uni.classroom.model.Assignment;
import com.uni.classroom.model.Student;
import com.uni.classroom.model.StudentAssignment;
import com.uni.classroom.repository.StudentAssignmentRepository;
import com.uni.classroom.repository.StudentRepository;

@Service
public class StudentAssignmentService {
	
	@Autowired
	private StudentAssignmentRepository studentAssignmentRepository;
	
	@Autowired
	private AssignmentService assignmentService;
	
	@Autowired
	private StudentService studentService;
	
	public StudentAssignment save(StudentAssignment studentAssignment) {
		StudentAssignment savedStudentAssignment = studentAssignmentRepository.save(studentAssignment);
		return savedStudentAssignment;
	}
	
	public StudentAssignment findById(Long studentAssignmentId) {
		return studentAssignmentRepository.findById(studentAssignmentId).orElse(null);
	}
	
	public StudentAssignment findByStudentAssignment(Long assignmentId , Long studentId) {
		return studentAssignmentRepository.findByAssignmentIdAndStudentId( assignmentId, studentId);
	}
}
