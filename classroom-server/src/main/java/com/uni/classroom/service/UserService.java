package com.uni.classroom.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uni.classroom.dto.RegisterStudentDTO;
import com.uni.classroom.dto.RegisterTeacherDTO;
import com.uni.classroom.model.IUser;
import com.uni.classroom.model.Student;
import com.uni.classroom.model.Teacher;
import com.uni.classroom.repository.StudentRepository;
import com.uni.classroom.repository.TeacherRepository;

@Service
public class UserService {

	@Autowired
	private StudentRepository studentRepository;
	
	@Autowired
	private TeacherRepository teacherRepository;

	
//	public List<User> getAll() {
//		return userRepository.findAll();
//	}

	public IUser findByEmail(String email) {
		IUser student = studentRepository.findByEmail(email);
		IUser teacher = teacherRepository.findByEmail(email);
		if(student != null) {
			return student;
		}
		else {
			return teacher;
		}
	}

}
