package com.uni.classroom.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uni.classroom.model.Student;
import com.uni.classroom.model.Course;
import com.uni.classroom.repository.StudentRepository;

@Service
public class StudentService {

	@Autowired
	private StudentRepository studentRepository;
	
	public Student save(Student student) {
		Student savedStudent = studentRepository.save(student);
		return savedStudent;
	}
	
	public List<Course> getCourses(Student student){
		List<Course> courses = student.getCourses();
		return courses;
	}

	public Student findById(Long id) {
		return studentRepository.findById(id).orElse(null);
	}
	
	public Student findByEmail(String email) {
		return studentRepository.findByEmail(email);
	}

}
