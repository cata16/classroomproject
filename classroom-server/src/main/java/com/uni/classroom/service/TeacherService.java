package com.uni.classroom.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uni.classroom.model.Course;
import com.uni.classroom.model.Student;
import com.uni.classroom.model.Teacher;
import com.uni.classroom.repository.TeacherRepository;

@Service
public class TeacherService {

	@Autowired
	private TeacherRepository teacherRepository;
	
	public Teacher save(Teacher teacher) {
		Teacher savedTeacher = teacherRepository.save(teacher);
		return savedTeacher;
	}
	
	public Teacher findById(Long teacherId) {
		return teacherRepository.findById(teacherId).orElse(null);
	}
	
	public Teacher findByEmail(String email) {
		return teacherRepository.findByEmail(email);
	}
}
