package com.uni.classroom.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.uni.classroom.configuration.IConfiguration;

@Component
public class Configuration implements IConfiguration{

	@Value("${path.home.saving}")
	private String savingFilePath;
	
	@Override
	public String getFileSavePath() {
		return savingFilePath;
	}
	
	
}
