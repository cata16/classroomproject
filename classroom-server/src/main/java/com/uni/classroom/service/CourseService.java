package com.uni.classroom.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uni.classroom.model.Assignment;
import com.uni.classroom.model.Course;
import com.uni.classroom.model.Student;
import com.uni.classroom.model.StudentAssignment;
import com.uni.classroom.repository.CourseRepository;
import com.uni.classroom.repository.StudentRepository;
import com.uni.classroom.utils.AssignmentStatus;

@Service
public class CourseService {

	@Autowired
	private CourseRepository courseRepository;

	@Autowired
	private StudentRepository studentRepository;
	
	@Autowired
	private AssignmentService assignmentService;

	@Autowired
	private StudentAssignmentService studentAssignmentService;

	public Course findByName(String name) {
		return courseRepository.findByName(name);
	}

	public Course save(Course course) {
		return courseRepository.save(course);
	}

	public Course findById(Long courseId) {
		return courseRepository.findById(courseId).orElse(null);
	}

	public boolean leaveStudent(Course course, Long studentId) {
		Student student = studentRepository.findById(studentId).orElse(null);
		if (student == null) {
			return false;
		}
		
		course.leaveStudent(student);

		save(course);

		return true;
	}

	public boolean assignStudent(Course course, Long studentId) {
		Student student = studentRepository.findById(studentId).orElse(null);
		if (student == null) {
			return false;
		}

		course.assignStudent(student);

		save(course);

		return true;
	}

	public List<Course> findAll() {
		return courseRepository.findAll();
	}

	public Course findByCode(Long courseCode) {
		return courseRepository.findByCode(courseCode);
	}

	public boolean joinStudent(Course course, Long studentId) {
		Student student = studentRepository.findById(studentId).orElse(null);
		if (student == null) {
			return false;
		}
		course.joinStudent(student);

		save(course);
		
		List<Assignment> courseAssignments = assignmentService.findByCourseId(course.getId());
		
		for (Assignment assignment : courseAssignments) {
			StudentAssignment studentAssignment = new StudentAssignment();
			studentAssignment.setAssignment(assignment);
			studentAssignment.setStudent(student);
			studentAssignment.setStatus(AssignmentStatus.ASSIGNED);
			studentAssignmentService.save(studentAssignment);
		}

		return true;
	}

	public List<Student> getAllStudents(Long courseId) {
		Course course = findById(courseId);
		List<Student> allStudents = new ArrayList<Student>();
		allStudents = course.getStudents();
		return allStudents;
	}
	
	
	public void deleteCourse(Course course) {
		
		courseRepository.delete(course);

	}

	public List<Course> findByStudentId(Long studentId) {
		return courseRepository.findByStudents_Id(studentId);
	}

	public List<Course> findByTeacherId(Long teacherId) {
		return courseRepository.findByTeacherId(teacherId);
	}

}
