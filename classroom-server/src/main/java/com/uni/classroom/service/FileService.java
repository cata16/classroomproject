package com.uni.classroom.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.uni.classroom.configuration.IConfiguration;
import com.uni.classroom.model.StudentAssignment;
import com.uni.classroom.utils.FileBuilder;

@Service
public class FileService {

	@Autowired
	private IConfiguration config;
	
	private String savingFilePath;

	@Autowired
	private StudentAssignmentService studentAssignmentService;

	@Autowired
	private FileBuilder fileBuilder;

	@Autowired
	private com.uni.classroom.repository.FileRepository fileRepository;

	public com.uni.classroom.model.File upload(String fileName, InputStream fileStream, Long studentId, Long assignmentId)
			throws IOException {
		if(assignmentId == null || studentId == null) {
			return null;
		}
		StudentAssignment studentAssignment = studentAssignmentService.findByStudentAssignment(assignmentId, studentId);
		if(studentAssignment == null) {
			return null;
		}
		
		File file = getFileWithAbsoutePath(fileName);
		com.uni.classroom.model.File dbFile = fileBuilder.build(file);
		dbFile.setStudentAssignment(studentAssignment);
		com.uni.classroom.model.File savedFile = fileRepository.save(dbFile);
		File renamedFile = getFileWithAbsoutePath(savedFile.getId()+ "."+ savedFile.getExtension());
		OutputStream out = new FileOutputStream(renamedFile, false);
		IOUtils.copy(fileStream, out);
		fileStream.close();
		out.close();
		return savedFile;

	}

	private File getFileWithAbsoutePath(String fileName) {
		String absolutePath = getAbsolutePath(fileName);
		return new File(absolutePath);
	}

	private String getAbsolutePath(String fileName) {
		StringBuilder sb = new StringBuilder(savingFilePath);

		sb.append(File.separator);
		sb.append(fileName);

		return sb.toString();

	}

	public File getFileById(Long fileId) {
		com.uni.classroom.model.File dbFile = fileRepository.findById(fileId).orElse(null);
		if(dbFile == null) {
			return null;
		}
		
		File diskFile = getDiskFile(dbFile);
		return diskFile;
	}
	
	private File getDiskFile(com.uni.classroom.model.File dbFile) {
		savingFilePath = config.getFileSavePath();
		return new File(savingFilePath,
				dbFile.getId() + "." + dbFile.getExtension());
	}

	public com.uni.classroom.model.File findById(Long fileId) {
		return fileRepository.findById(fileId).orElse(null);
	}

	public com.uni.classroom.model.File deleteById(Long fileId) {
		com.uni.classroom.model.File dbFile = fileRepository.findById(fileId).orElse(null);
		if (dbFile == null) {
			return null;
		}

		File diskFile = getDiskFile(dbFile);

		if (diskFile.exists()) {
			boolean isDeleted = false;
			isDeleted = diskFile.delete();
			if (isDeleted == false) {
				return null;
			}
		}

		fileRepository.deleteById(fileId);
		return dbFile;
	}
}
