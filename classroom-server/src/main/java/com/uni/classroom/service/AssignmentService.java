package com.uni.classroom.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uni.classroom.model.Assignment;
import com.uni.classroom.repository.AssignmentRepository;

@Service
public class AssignmentService {
	
	@Autowired
	private AssignmentRepository assignmentRepository;
	
	public Assignment findByName(String name) {
		return assignmentRepository.findByTitle(name);
	}

	public Assignment save(Assignment assignment) {
		return assignmentRepository.save(assignment);
	}
	
	public Assignment findById(Long assignmentId) {
		return assignmentRepository.findById(assignmentId).orElse(null);
	}

	public List<Assignment> findByCourseId(Long id) {
		return assignmentRepository.findByCourseId(id);
	}
	
	
}
