package com.uni.classroom.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uni.classroom.dto.RegisterStudentDTO;
import com.uni.classroom.dto.RegisterTeacherDTO;
import com.uni.classroom.mapper.StudentMapper;
import com.uni.classroom.mapper.TeacherMapper;
import com.uni.classroom.model.Student;
import com.uni.classroom.model.Teacher;
import com.uni.classroom.payload.response.MessageResponse;
import com.uni.classroom.service.StudentService;
import com.uni.classroom.service.TeacherService;
import com.uni.classroom.service.UserService;
import com.uni.classroom.utils.ResponseMessages;

@CrossOrigin
@RestController
@RequestMapping("/registration")
public class RegistrationController {

	@Autowired
	private TeacherService teacherService;

	@Autowired
	private TeacherMapper teacherMapper;

	@Autowired
	private StudentService studentService;

	@Autowired
	private StudentMapper studentMapper;

	@Autowired
	private UserService userService;

	@PostMapping("/student")
	public ResponseEntity<?> registerStudent(@Valid @RequestBody RegisterStudentDTO registerStudentDTO) {
		if (registerStudentDTO.getPassword().equals(registerStudentDTO.getMatchingPassword())) {
			Student student = studentMapper.map(registerStudentDTO);

			if (userService.findByEmail(registerStudentDTO.getEmail()) != null) {
				return ResponseEntity.status(HttpStatus.CONFLICT)
						.body(new MessageResponse(ResponseMessages.EMAIL_EXISTS.getValue()));
			}

			Student savedStudent = studentService.save(student);

			if (savedStudent == null) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
			}

			return ResponseEntity.created(null).body(savedStudent);
		} else {
			return ResponseEntity.status(HttpStatus.CONFLICT)
					.body(new MessageResponse(ResponseMessages.PASS_MATCH.getValue()));
		}
	}

	@PostMapping("/teacher")
	public ResponseEntity<?> registerTeacher(@Valid @RequestBody RegisterTeacherDTO registerTeacherDTO) {
		if (registerTeacherDTO.getPassword().equals(registerTeacherDTO.getMatchingPassword())) {
			Teacher teacher = teacherMapper.map(registerTeacherDTO);

			if (userService.findByEmail(registerTeacherDTO.getEmail()) != null) {
				return ResponseEntity.status(HttpStatus.CONFLICT)
						.body(new MessageResponse(ResponseMessages.EMAIL_EXISTS.getValue()));
			}

			Teacher savedTeacher = teacherService.save(teacher);

			if (savedTeacher == null) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
			}

			return ResponseEntity.created(null).body(savedTeacher);
		} else {
			return ResponseEntity.status(HttpStatus.CONFLICT)
					.body(new MessageResponse(ResponseMessages.PASS_MATCH.getValue()));
		}
	}
}
