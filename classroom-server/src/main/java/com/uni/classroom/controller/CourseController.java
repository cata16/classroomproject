package com.uni.classroom.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uni.classroom.dto.CourseDTO;
import com.uni.classroom.dto.StudentDTO;
import com.uni.classroom.mapper.CourseMapper;
import com.uni.classroom.mapper.StudentMapper;
import com.uni.classroom.model.Course;
import com.uni.classroom.model.Student;
import com.uni.classroom.model.Teacher;
import com.uni.classroom.payload.response.MessageResponse;
import com.uni.classroom.service.CourseService;
import com.uni.classroom.service.StudentService;
import com.uni.classroom.service.TeacherService;
import com.uni.classroom.utils.ResponseMessages;

@CrossOrigin
@RestController
@RequestMapping("/course")
public class CourseController {

	@Autowired
	private CourseMapper courseMapper;

	@Autowired
	private CourseService courseService;

	@Autowired
	private StudentService studentService;

	@Autowired
	private TeacherService teacherService;

	@Autowired
	private StudentMapper studentMapper;

//	@GetMapping("/")
//	public ResponseEntity<? extends Object> getClasses() {
//		List<String> classes = new ArrayList<>();
//		classes.add("class1");
//		classes.add("class2");
//		classes.add("class3");
//		return ResponseEntity.ok(classes);
//	}

	@PostMapping("/")
	public ResponseEntity<?> create(@RequestBody CourseDTO courseDTO) {
		Course course = courseMapper.map(courseDTO);
		if (courseService.findByName(courseDTO.getName()) != null) {
			return ResponseEntity.status(HttpStatus.CONFLICT)
					.body(new MessageResponse(ResponseMessages.COURSE_NAME_EXISTS.getValue()));
		}

		course.setCode(Long.valueOf(0));
		Course savedCourse = courseService.save(course);
		if (savedCourse == null) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		
		savedCourse.setCode(savedCourse.getId());
		savedCourse = courseService.save(savedCourse);
		
		CourseDTO savedCourseDTO = courseMapper.mapCourse(savedCourse);

		return ResponseEntity.created(null).body(savedCourseDTO);
	}

	@PostMapping("{theacherId}/create")
	public ResponseEntity<?> create(@RequestBody CourseDTO courseDTO, @PathVariable("theacherId") Long theacherId) {
		Course course = courseMapper.map(courseDTO);
		course.setTeacher(teacherService.findById(theacherId));
		if (courseService.findByName(courseDTO.getName()) != null) {
			return ResponseEntity.status(HttpStatus.CONFLICT)
					.body(new MessageResponse(ResponseMessages.COURSE_NAME_EXISTS.getValue()));
		}

		course.setCode(Long.valueOf(0));
		Course savedCourse = courseService.save(course);
		if (savedCourse == null) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		savedCourse.setCode(savedCourse.getId());
		savedCourse = courseService.save(savedCourse);
		CourseDTO savedCourseDTO = courseMapper.mapCourse(savedCourse);

		return ResponseEntity.created(null).body(savedCourseDTO);
	}

	@PostMapping("/{courseId}/assign/{studentId}")
	public ResponseEntity<?> assignStudent(@PathVariable("courseId") Long courseId,
			@PathVariable("studentId") Long studentId) {
		Course course = courseService.findById(courseId);
		if (course == null) {
			return ResponseEntity.notFound().build();
		}
		boolean assigned = courseService.assignStudent(course, studentId);
		if (assigned == false) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		return ResponseEntity.ok().build();
	}

	@PostMapping("/{courseId}/assign")
	public ResponseEntity<?> assignStudentByTeacher(@PathVariable("courseId") Long courseId,
			@RequestBody String email) {
		Course course = courseService.findById(courseId);
		if (course == null) {
			return ResponseEntity.notFound().build();
		}
		boolean assigned = courseService.assignStudent(course, studentService.findByEmail(email).getId());
		if (assigned == false) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		return ResponseEntity.ok().build();
	}

	@PostMapping("/{courseCode}/join/{studentId}")
	public ResponseEntity<?> joinStudent(@PathVariable("courseCode") Long courseCode,
			@PathVariable("studentId") Long studentId) {
		Course course = courseService.findByCode(courseCode);
		if (course == null) {
			return ResponseEntity.notFound().build();
		}
		boolean joined = courseService.joinStudent(course, studentId);
		if (joined == false) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		return ResponseEntity.ok().build();
	}

	@GetMapping("/")
	public ResponseEntity<?> getAllCourses() {
		List<Course> courses = courseService.findAll();

		List<CourseDTO> coursesDto = courseMapper.map(courses);

		return ResponseEntity.ok(coursesDto);
	}

	@GetMapping("/{courseId}")
	public ResponseEntity<?> getCourseById(@PathVariable("courseId") Long courseId) {
		Course course = courseService.findById(courseId);

		CourseDTO courseDto = courseMapper.mapCourse(course);

		return ResponseEntity.ok(courseDto);
	}

	@GetMapping("/student/{studentId}")
	public ResponseEntity<?> getAllCoursesStudent(@PathVariable("studentId") Long studentId) {
		List<Course> courses = courseService.findByStudentId(studentId);

		List<CourseDTO> coursesDto = courseMapper.map(courses);

		return ResponseEntity.ok(coursesDto);
	}

	@GetMapping("/teacher/{teacherId}")
	public ResponseEntity<?> getAllClassesTeacher(@PathVariable("teacherId") Long teacherId) {
		List<Course> courses = courseService.findByTeacherId(teacherId);

		List<CourseDTO> coursesDto = courseMapper.map(courses);

		return ResponseEntity.ok(coursesDto);
	}

	@GetMapping("/{courseId}/{studentId}/leave")
	public ResponseEntity<?> leaveCourse(@PathVariable("courseId") Long courseId,
			@PathVariable("studentId") Long studentId) {

		Course course = courseService.findById(courseId);
		if (course == null) {
			return ResponseEntity.notFound().build();
		}
		boolean leave = courseService.leaveStudent(course, studentId);
		if (leave == false) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		return ResponseEntity.ok().build();
	}

	@GetMapping("/{courseId}/people")
	public ResponseEntity<?> getAllCoursesPeople(@PathVariable("courseId") Long courseId) {
		Course course = courseService.findById(courseId);

		List<Student> students = course.getStudents();

		List<StudentDTO> studentDto = studentMapper.map(students);

		return ResponseEntity.ok(studentDto);
	}

	@GetMapping("/{courseId}/delete")
	public ResponseEntity<?> deleteCourse(@PathVariable("courseId") Long courseId,
			@PathVariable("teacherId") Long studentId) {

		Course course = courseService.findById(courseId);
		if (course == null) {
			return ResponseEntity.notFound().build();
		}
		courseService.deleteCourse(course);
		
		return ResponseEntity.ok().build();
	}
}