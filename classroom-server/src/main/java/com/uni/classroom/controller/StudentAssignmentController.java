package com.uni.classroom.controller;

import java.time.LocalDateTime;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uni.classroom.dto.StudentAssignmentDTO;
import com.uni.classroom.mapper.StudentAssignmentMapper;
import com.uni.classroom.model.File;
import com.uni.classroom.model.StudentAssignment;
import com.uni.classroom.repository.StudentAssignmentRepository;
import com.uni.classroom.service.StudentAssignmentService;
import com.uni.classroom.utils.AssignmentStatus;

@CrossOrigin
@RestController
@RequestMapping("/assignmentDetails")
public class StudentAssignmentController {

	@Autowired
	private StudentAssignmentRepository studentAssignmentRepository;
	
	@Autowired
	private StudentAssignmentService studentAssignmentService;

	@Autowired
	private StudentAssignmentMapper studentAssignmentMapper;
	
//	@Autowired
//	private FileService fileService;

	@PostMapping("/{studentAssignmentId}/addGrade")
	public ResponseEntity<?> AddGrade(@PathVariable("studentAssignmentId") Long studentAssignmentId,@Valid @RequestBody int grade) {

		studentAssignmentRepository.updateGrade(grade, studentAssignmentId);
		studentAssignmentRepository.updateStatus(AssignmentStatus.GRADED, studentAssignmentId);

		return ResponseEntity.ok().build();
	}

	@PostMapping("/{studentAssignmentId}/addFeedback")
	public ResponseEntity<?> AddFeedback(@PathVariable("studentAssignmentId") Long studentAssignmentId,@Valid @RequestBody String feedback) {

		studentAssignmentRepository.updateFeedback(feedback, studentAssignmentId);

		return ResponseEntity.ok().build();
	}

	@PostMapping("/{studentAssignmentId}/addFiles")
	public ResponseEntity<?> AddFiles(@PathVariable("studentAssignmentId") Long studentAssignmentId,@Valid @RequestBody String path) {
		
		File file = new File();
//		file.setPath(path);
		file.setDate( LocalDateTime.now() );
		file.setStudentAssignment( studentAssignmentRepository.getById(studentAssignmentId) );
//		fileService.save(file);
		
		if( LocalDateTime.now().isBefore(studentAssignmentRepository.getById(studentAssignmentId).getAssignment().getDueDate()) )
		{
			studentAssignmentRepository.updateStatus(AssignmentStatus.TURNED_IN, studentAssignmentId);
		}
		else {
			studentAssignmentRepository.updateStatus(AssignmentStatus.TURNED_IN_LATE, studentAssignmentId);
		}

		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/{assignmentId}/{studentId}")
	public ResponseEntity<?> getAssignmentDetails(@PathVariable("assignmentId") Long assignmentId,@PathVariable("studentId") Long studentId){
		
		StudentAssignment studentAssignment = studentAssignmentService.findByStudentAssignment( assignmentId , studentId );
		
		StudentAssignmentDTO studentAssignmentDto = studentAssignmentMapper.mapStudentAssignment(studentAssignment);
		
		return ResponseEntity.ok(studentAssignmentDto);
	}
}
