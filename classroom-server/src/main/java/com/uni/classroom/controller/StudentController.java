package com.uni.classroom.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uni.classroom.dto.AssignmentDTO;
import com.uni.classroom.dto.CourseDTO;
import com.uni.classroom.dto.PasswordDTO;
import com.uni.classroom.dto.StudentDTO;
import com.uni.classroom.dto.StudentProfileDTO;
import com.uni.classroom.mapper.StudentMapper;
import com.uni.classroom.model.Student;
import com.uni.classroom.payload.response.MessageResponse;
import com.uni.classroom.service.StudentService;
import com.uni.classroom.service.UserService;
import com.uni.classroom.utils.ResponseMessages;

@CrossOrigin
@RestController
@RequestMapping("/student")
public class StudentController {

	@Autowired
	private StudentService studentService;
	
	@Autowired
	private StudentMapper studentMapper;

	@Autowired
	private UserService userService;	

	@PostMapping("/")
	public ResponseEntity<?> create(@Valid @RequestBody StudentDTO studentDTO) {
		Student student = studentMapper.map(studentDTO);
		
		if(userService.findByEmail(studentDTO.getEmail()) != null) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(new MessageResponse(ResponseMessages.EMAIL_EXISTS.getValue()));
		}
		
		Student savedStudent = studentService.save(student);
		
		if(savedStudent == null) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		
		return ResponseEntity.created(null).body(savedStudent);
	}	
	
	@PostMapping("/{studentId}/changePassword")
	public ResponseEntity<?> changePassword(@RequestBody PasswordDTO passwordDTO, @PathVariable("studentId") Long studentId) {
		
	}	
	
	
}

