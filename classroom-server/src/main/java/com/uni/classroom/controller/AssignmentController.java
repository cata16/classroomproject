package com.uni.classroom.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.FileItemIterator;
import org.apache.tomcat.util.http.fileupload.FileItemStream;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.util.Streams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.uni.classroom.dto.AssignmentDTO;
import com.uni.classroom.dto.FileDTO;
import com.uni.classroom.mapper.AssignmentMapper;
import com.uni.classroom.mapper.FileMapper;
import com.uni.classroom.model.Assignment;
import com.uni.classroom.model.Course;
import com.uni.classroom.model.File;
import com.uni.classroom.model.Student;
import com.uni.classroom.model.StudentAssignment;
import com.uni.classroom.payload.response.MessageResponse;
import com.uni.classroom.service.AssignmentService;
import com.uni.classroom.service.CourseService;
import com.uni.classroom.service.FileService;
import com.uni.classroom.service.StudentAssignmentService;
import com.uni.classroom.utils.AssignmentStatus;
import com.uni.classroom.utils.ResponseMessages;

@CrossOrigin
@RestController
@RequestMapping("/assignment")
public class AssignmentController {
	private static final String USER_ID = "userId";
	private static final String FILE = "file";
	private static final String ASSIGNMENT_ID = "assignmentId";
	private static final String PARENT_ID = "parentId";
	

	@Autowired
	private AssignmentMapper assignmentMapper;

	@Autowired
	private AssignmentService assignmentService;

	@Autowired
	private CourseService courseService;

	@Autowired
	private StudentAssignmentService studentAssignmentService;

	@Autowired
	private FileService fileService;
	
	@Autowired
	private FileMapper fileMapper;

	@PostMapping("/{courseId}/create")
	public ResponseEntity<?> create(@RequestBody AssignmentDTO assignmentDTO, @PathVariable("courseId") Long courseId) {
		Assignment assignment = assignmentMapper.map(assignmentDTO);

		assignment.setCourse(courseService.findById(courseId));

		if (assignmentService.findByName(assignmentDTO.getTitle()) != null) {
			return ResponseEntity.status(HttpStatus.CONFLICT)
					.body(new MessageResponse(ResponseMessages.ASSIGNMENT_NAME_EXISTS.getValue()));
		}

		Assignment savedAssignment = assignmentService.save(assignment);
		if (savedAssignment == null) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

		// assign the assignment to every student
		List<Student> listStudent = courseService.getAllStudents(courseId);
		for (int i = 0; i < listStudent.size(); i++) {
			StudentAssignment studentAssignment = new StudentAssignment();
			studentAssignment.setAssignment(savedAssignment);
			studentAssignment.setStudent(listStudent.get(i));
			studentAssignment.setStatus(AssignmentStatus.ASSIGNED);
			studentAssignmentService.save(studentAssignment);
		}

		AssignmentDTO savedAssignmentDTO = assignmentMapper.mapAssignment(savedAssignment);

		return ResponseEntity.created(null).body(savedAssignmentDTO);
	}

	@GetMapping("/{courseId}")
	public ResponseEntity<?> getAllAssignmentsCourse(@PathVariable("courseId") Long courseId) {
		Course course = courseService.findById(courseId);

		List<Assignment> assignments = course.getAssignments();

		List<AssignmentDTO> assignmentDto = assignmentMapper.map(assignments);

		return ResponseEntity.ok(assignmentDto);
	}
	
	@GetMapping("/assignments/{assignmentId}")
	public ResponseEntity<?> getAssignmentById(@PathVariable("assignmentId") Long assignmentId) {
		Assignment assignment = assignmentService.findById(assignmentId);
		
		if(assignment == null) {
			return ResponseEntity.notFound().build();
		}
		
		AssignmentDTO assignmentDto = assignmentMapper.mapAssignment(assignment);

		return ResponseEntity.ok(assignmentDto);
	}
	
	@PostMapping("/")
	public ResponseEntity<?> handleUpload(HttpServletRequest request) throws Exception {
		// TODO: add exists check, save to database
		String fileName = null;
		InputStream fileStream = null;
		Long userId = null;
		Long assignmentId = null;
		ServletFileUpload upload = new ServletFileUpload();
		FileItemIterator iterStream = upload.getItemIterator(request);
		File dbFile = null;
		try {
			while (iterStream.hasNext()) {
				FileItemStream item = iterStream.next();
				String fieldName = item.getFieldName();
				InputStream stream = item.openStream();
				switch (fieldName) {
				case USER_ID:
					userId = Long.valueOf(Streams.asString(stream));
					break;
				case ASSIGNMENT_ID:
					assignmentId = Long.valueOf(Streams.asString(stream));
					break;
				case FILE:
					fileStream = item.openStream();
					fileName = item.getName();
					dbFile = fileService.upload(fileName, fileStream, userId, assignmentId);
					break;
				}
			}

		} catch (Exception e) {
			 e.printStackTrace(System.out);
		}
		if(dbFile == null) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		return ResponseEntity.ok().body(fileMapper.map(dbFile));

	}
	
	@PostMapping(value = "/{fileId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StreamingResponseBody> download(final HttpServletResponse response, @PathVariable Long fileId) {

		response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
		final java.io.File file = fileService.getFileById(fileId);

		if (file == null || file.exists() == false) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}

			try {
				response.setContentType(Files.probeContentType(file.toPath()));
			} catch (IOException e) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
			}
			response.setHeader("Content-Disposition", "attachment;filename=" + file.getName());

			StreamingResponseBody stream = outputStream -> {
				int bytesRead;
				byte[] buffer = new byte[1024];
				InputStream inputStream = new FileInputStream(file);
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}
				inputStream.close();
			};
			return ResponseEntity.ok().body(stream);

//		StreamingResponseBody stream = out -> {
//			response.setContentType("application/zip");
//			response.setHeader("Content-Disposition", "attachment;filename=" + dbFile.getName() + ".zip");
//
//			final ZipOutputStream zipOut = new ZipOutputStream(response.getOutputStream());
//			zipDir(dbFile, zipOut, dbFile.toPath());
//			zipOut.close();
//		};
//		return ResponseEntity.ok().body(stream);
	}
	
	@DeleteMapping(value = "/{fileId}")
	public ResponseEntity<?> delete(@PathVariable Long fileId) {

		File dbFile = fileService.findById(fileId);
		if (dbFile == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		File deletedFile = null;
		
		deletedFile = fileService.deleteById(fileId);
		
		if (deletedFile == null) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		FileDTO deletedFileDTO = fileMapper.map(deletedFile);
		return ResponseEntity.ok(deletedFileDTO);

	}
	
}
