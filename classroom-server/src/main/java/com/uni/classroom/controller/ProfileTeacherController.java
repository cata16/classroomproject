package com.uni.classroom.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uni.classroom.dto.TeacherProfileDTO;
import com.uni.classroom.mapper.TeacherMapper;
import com.uni.classroom.model.Teacher;
import com.uni.classroom.service.TeacherService;

@CrossOrigin
@RestController
@RequestMapping("/profileTeacher")
public class ProfileTeacherController {

	@Autowired
	private TeacherService teacherService;
	
	@Autowired
	private TeacherMapper teacherMapper;

	@GetMapping("/{teacherId}")
	public ResponseEntity<?> getProfileDetails(@PathVariable("teacherId") Long teacherId){
		Teacher teacher = teacherService.findById(teacherId);
		
		TeacherProfileDTO teacherProfileDto = teacherMapper.mapTeacherProfile(teacher);
		
		return ResponseEntity.ok(teacherProfileDto);
	}
	
	
	
}
