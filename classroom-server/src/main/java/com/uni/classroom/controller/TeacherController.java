package com.uni.classroom.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uni.classroom.dto.StudentProfileDTO;
import com.uni.classroom.dto.TeacherDTO;
import com.uni.classroom.dto.TeacherProfileDTO;
import com.uni.classroom.mapper.StudentMapper;
import com.uni.classroom.mapper.TeacherMapper;
import com.uni.classroom.model.Student;
import com.uni.classroom.model.Teacher;
import com.uni.classroom.payload.response.MessageResponse;
import com.uni.classroom.service.StudentService;
import com.uni.classroom.service.TeacherService;
import com.uni.classroom.service.UserService;
import com.uni.classroom.utils.ResponseMessages;

@CrossOrigin
@RestController
@RequestMapping("/teacher")
public class TeacherController {

	@Autowired
	private TeacherService teacherService;
	
	@Autowired
	private TeacherMapper teacherMapper;

	@Autowired
	private UserService userService;	

	@PostMapping("/")
	public ResponseEntity<?> create(@Valid @RequestBody TeacherDTO teacherDTO) {
		Teacher teacher = teacherMapper.map(teacherDTO);
		
		if(userService.findByEmail(teacherDTO.getEmail()) != null) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(new MessageResponse(ResponseMessages.EMAIL_EXISTS.getValue()));
		}
		
		Teacher savedTeacher = teacherService.save(teacher);
		
		if(savedTeacher == null) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		
		return ResponseEntity.created(null).body(savedTeacher);
	}

	@GetMapping("/{teacherId}")
	public ResponseEntity<?> getProfileDetails(@PathVariable("teacherId") Long teacherId){
		Teacher teacher = teacherService.findById(teacherId);
		
		TeacherProfileDTO teacherProfileDto = teacherMapper.mapTeacherProfile(teacher);
		
		return ResponseEntity.ok(teacherProfileDto);
	}
}
