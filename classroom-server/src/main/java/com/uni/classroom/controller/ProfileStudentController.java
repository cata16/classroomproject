package com.uni.classroom.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uni.classroom.dto.StudentProfileDTO;
import com.uni.classroom.mapper.StudentMapper;
import com.uni.classroom.model.Student;
import com.uni.classroom.service.StudentService;

@CrossOrigin
@RestController
@RequestMapping("/profileStudent")
public class ProfileStudentController {

	@Autowired
	private StudentService studentService;
	
	@Autowired
	private StudentMapper studentMapper;

	@GetMapping("/{studentId}")
	public ResponseEntity<?> getProfileDetails(@PathVariable("studentId") Long studentId){
		Student student = studentService.findById(studentId);
		
		StudentProfileDTO studentProfileDto = studentMapper.mapStudentProfile(student);
		
		return ResponseEntity.ok(studentProfileDto);
	}
	
	
	
}
