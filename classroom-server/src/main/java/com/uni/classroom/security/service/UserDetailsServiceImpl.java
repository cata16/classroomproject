package com.uni.classroom.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uni.classroom.model.IUser;
import com.uni.classroom.service.UserService;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	private UserService userService;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		IUser user = userService.findByEmail(username);
		if(user == null) {
			throw new UsernameNotFoundException("User Not Found with email: " + username);
		}

		return UserDetailsImpl.build(user);
	}

}
