package com.uni.classroom.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Teacher implements IUser {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String email;
	private String password;
	private String phoneNumber;
	private String fullName;
	private String department;
	private String university;
	
	@Override
	public String getRole() {
		return "Teacher";
	}
	
	@OneToMany(mappedBy = "teacher", cascade = CascadeType.ALL)
	private List<Course> classes;
}
