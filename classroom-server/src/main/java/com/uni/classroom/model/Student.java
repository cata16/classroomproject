package com.uni.classroom.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Student implements IUser {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String email;
	private String password;
	private String phoneNumber;
	private String fullName;
	private String university;
	private String groupStudy;
	private int yearOfStudy;
	
	@Override
	public String getRole() {
		return "Student";
	}
	
	@ManyToMany(mappedBy = "students")
    private List<Course> courses = new ArrayList<Course>();
	
	@OneToMany(mappedBy = "student")
	List<StudentAssignment> studentAssignments;
	
	public List<Course> getCourses()
	{
		return courses;
	}
}
