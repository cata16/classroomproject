package com.uni.classroom.model;

public interface IUser {

	String getRole();

	Long getId();

	String getEmail();

	String getPassword();

}
