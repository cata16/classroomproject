package com.uni.classroom.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Course {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	//@Column(unique = true)
	private Long code;
	
	@ManyToOne
	@JoinColumn(name = "teacher_id")
	private Teacher teacher;
	
	@OneToMany(mappedBy = "course", cascade = CascadeType.ALL)
	private List<Assignment> assignments;
	
	@ManyToMany(cascade = {
			CascadeType.PERSIST,
			CascadeType.MERGE
	})
	@JoinTable(
			  name = "course_student", 
			  joinColumns = @JoinColumn(name = "course_id"), 
			  inverseJoinColumns = @JoinColumn(name = "student_id"))
    private List<Student> students = new ArrayList<Student>();

	
	public void assignStudent(Student student) {
		if(students == null) {
			students = new ArrayList<Student>();
		}
		
		//students.add(student);
		this.students.add(student);
		//student.getCourses().add(this);
		
	}
	
	public void leaveStudent(Student student) {
		if(students == null) {
			students = new ArrayList<Student>();
		}
		
		//students.add(student);
		this.students.remove(student);
	}

	public void joinStudent(Student student) {
		if(students == null) {
			students = new ArrayList<Student>();
		}
		
		students.add(student);
		
	}	
	
	public List<Student> getStudents()
	{
		return students;
	}
}
