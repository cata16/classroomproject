package com.uni.classroom.utils;

public enum AssignmentStatus {
	GRADED,
	TURNED_IN,
	TURNED_IN_LATE,
	ASSIGNED,
	
}
