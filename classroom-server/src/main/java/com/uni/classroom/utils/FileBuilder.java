package com.uni.classroom.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.uni.classroom.model.File;

@Component
public class FileBuilder {

	@Value("${path.home.saving}")
	private String baseSavingFilePath;
	
	public File build(java.io.File file) {
		if(file == null) {
			return null;
		}
		
		File dbFile = new File();
		
//		dbFile.setDirectory(file.isDirectory());
//		if(file.isFile()) {
		dbFile.setExtension(FileUtils.getExtension(file));
//		}
		
		dbFile.setName(FileUtils.getName(file));
//		String path = FileUtils.createPathFromParentFile(parentFile); 
//		dbFile.setPath(path);
//		dbFile.setParentFile(parentFile);
		return dbFile;
	}
	
}
