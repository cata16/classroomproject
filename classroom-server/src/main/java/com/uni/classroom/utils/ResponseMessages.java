package com.uni.classroom.utils;

public enum ResponseMessages {
	EMAIL_EXISTS("Email already exists!"),
	PASS_MATCH("The password it's not the same!"), 
	COURSE_NAME_EXISTS(""),
	ASSIGNMENT_NAME_EXISTS("");
	
	private String value;
	
	ResponseMessages(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
}
