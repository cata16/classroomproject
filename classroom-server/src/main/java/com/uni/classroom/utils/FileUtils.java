package com.uni.classroom.utils;

import java.io.File;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

public class FileUtils {

	public static String getName(File file) {
		return FilenameUtils.getBaseName(file.getName());
	}

	public static String getExtension(File file) {
		return FilenameUtils.getExtension(file.getName()).toLowerCase();
	}

	public static String getPathForParent(File file, String baseFilePath) {
		if(file.isDirectory()) {
		return StringUtils
				.removeStart(
						FilenameUtils.getFullPathNoEndSeparator(file.getAbsolutePath())
						, baseFilePath + File.separator);
		} else {
			return StringUtils
					.removeStart(
							FilenameUtils.getFullPath(file.getAbsolutePath())
							, baseFilePath + File.separator);
		}
	}

}
