package com.uni.classroom.configuration;

public interface IConfiguration {
	
	public String getFileSavePath();
}
