package com.uni.classroom.mapper;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uni.classroom.dto.AssignmentDTO;
import com.uni.classroom.dto.StudentAssignmentDTO;
import com.uni.classroom.model.Assignment;
import com.uni.classroom.model.StudentAssignment;
import com.uni.classroom.service.AssignmentService;

@Component
public class StudentAssignmentMapper {
	
	private static ModelMapper mapper = new ModelMapper();
	
	public StudentAssignmentDTO mapStudentAssignment(StudentAssignment studentAssignment) {
		StudentAssignmentDTO mappedStudentAssignment = mapper.map(studentAssignment, StudentAssignmentDTO.class);
		mappedStudentAssignment.setDescription( studentAssignment.getAssignment().getDescription() );
		mappedStudentAssignment.setTitle( studentAssignment.getAssignment().getTitle() );
		return mappedStudentAssignment;
	}

}
