package com.uni.classroom.mapper;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeToken;
import org.modelmapper.spi.MappingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.uni.classroom.dto.RegisterStudentDTO;
import com.uni.classroom.dto.StudentDTO;
import com.uni.classroom.dto.StudentProfileDTO;
import com.uni.classroom.model.Student;

@Component
public class StudentMapper {

	private static ModelMapper mapper = new ModelMapper();
	
	@Autowired
	private PasswordEncoder encoder;

//	@Autowired
//	private RoleService roleService;

	private static final Type userDtoListType = new TypeToken<List<StudentDTO>>() {}.getType();

	@PostConstruct
	public void configMapper() {

//		Converter<Set<Role>, Set<String>> convertRoleToString = new Converter<Set<Role>, Set<String>>() {
//			public Set<String> convert(MappingContext<Set<Role>, Set<String>> context) {
//				return context.getSource().stream().map(role -> role.getName().getValue()).collect(Collectors.toSet());
//			}
//		};
		
		Converter<String, String> encodePassword = new Converter<String, String>() {

			public String convert(MappingContext<String, String> context) {
				return encoder.encode(context.getSource());
			}
		};
//		Converter<Set<String>, Set<Role>> convertStringToRole = new Converter<Set<String>, Set<Role>>() {
//			public Set<Role> convert(MappingContext<Set<String>, Set<Role>> context) {
//				return roleService.getRolesByString(context.getSource());
//			}
//		};

//		PropertyMap<User, UserDTO> userToUserDTOMap = new PropertyMap<User, UserDTO>() {
//			protected void configure() {
//				using(convertRoleToString).map(source.getRoles()).setRoles(null);
//			}
//		};
//		
		PropertyMap<StudentDTO, Student> studentDTOToStudentMap = new PropertyMap<StudentDTO, Student>() {
			protected void configure() {
				using(encodePassword).map(source.getPassword()).setPassword(null);
//				using(convertStringToRole).map(source.getRoles()).setRoles(null);
			}
		};
		PropertyMap<RegisterStudentDTO, Student> registerStudentDTOToStudentMap = new PropertyMap<RegisterStudentDTO, Student>() {
			protected void configure() {
				using(encodePassword).map(source.getPassword()).setPassword(null);
//				using(convertStringToRole).map(source.getRoles()).setRoles(null);
			}
		};
//		mapper.addMappings(userToUserDTOMap);
		mapper.addMappings(studentDTOToStudentMap);
		mapper.addMappings(registerStudentDTOToStudentMap);
	}


	public Student map(StudentDTO studentDTO) {
		Student mappedStudent = mapper.map(studentDTO, Student.class);
		return mappedStudent;
	}
	
	public Student map(RegisterStudentDTO registerStudentDTO) {
		Student mappedStudent = mapper.map(registerStudentDTO, Student.class);
		return mappedStudent;
	}

	public List<StudentDTO> map(List<Student> students) {
		if (students == null || students.isEmpty()) {
			return Collections.emptyList();
		}

		return mapper.map(students, userDtoListType);
	}


	public StudentDTO mapStudent(Student student) {
		return mapper.map(student, StudentDTO.class);
	}
	
	public StudentProfileDTO mapStudentProfile(Student student){
		return mapper.map(student, StudentProfileDTO.class);
	}

}
