package com.uni.classroom.mapper;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Component;

import com.uni.classroom.dto.AssignmentDTO;
import com.uni.classroom.model.Assignment;

@Component
public class AssignmentMapper {
	
	private static ModelMapper mapper = new ModelMapper();

	private static final Type assignmentDtoListType = new TypeToken<List<AssignmentDTO>>() {}.getType();
	
	public Assignment map(AssignmentDTO assignmentDTO) {
		Assignment mappedAssignment = mapper.map(assignmentDTO, Assignment.class);
		return mappedAssignment;
	}
	
	public AssignmentDTO mapAssignment(Assignment assignment) {
		AssignmentDTO mappedAssignment = mapper.map(assignment, AssignmentDTO.class);
		return mappedAssignment;
	}

	public List<AssignmentDTO> map(List<Assignment> assignments) {
		if (assignments == null || assignments.isEmpty()) {
			return Collections.emptyList();
		}

		return mapper.map(assignments, assignmentDtoListType);
	}
}
