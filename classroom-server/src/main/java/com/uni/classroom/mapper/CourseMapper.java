package com.uni.classroom.mapper;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Component;

import com.uni.classroom.dto.CourseDTO;
import com.uni.classroom.model.Course;

@Component
public class CourseMapper {

	private static ModelMapper mapper = new ModelMapper();

	private static final Type coursesDtoListType = new TypeToken<List<CourseDTO>>() {}.getType();
	
	public Course map(CourseDTO courseDTO) {
		Course mappedCourse = mapper.map(courseDTO, Course.class);
		return mappedCourse;
	}

	public CourseDTO mapCourse(Course course) {
		CourseDTO mappedCourse = mapper.map(course, CourseDTO.class);
		return mappedCourse;
	}

	public List<CourseDTO> map(List<Course> courses) {
		if (courses == null || courses.isEmpty()) {
			return Collections.emptyList();
		}

		return mapper.map(courses, coursesDtoListType);
	}

	
}