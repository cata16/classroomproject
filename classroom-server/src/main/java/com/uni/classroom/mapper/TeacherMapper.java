package com.uni.classroom.mapper;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeToken;
import org.modelmapper.spi.MappingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.uni.classroom.dto.RegisterTeacherDTO;
import com.uni.classroom.dto.StudentProfileDTO;
import com.uni.classroom.dto.TeacherDTO;
import com.uni.classroom.dto.TeacherProfileDTO;
import com.uni.classroom.model.Student;
import com.uni.classroom.model.Teacher;

@Component
public class TeacherMapper {

	private static ModelMapper mapper = new ModelMapper();
	
	@Autowired
	private PasswordEncoder encoder;

//	@Autowired
//	private RoleService roleService;

	private static final Type userDtoListType = new TypeToken<List<TeacherDTO>>() {}.getType();

	@PostConstruct
	public void configMapper() {

//		Converter<Set<Role>, Set<String>> convertRoleToString = new Converter<Set<Role>, Set<String>>() {
//			public Set<String> convert(MappingContext<Set<Role>, Set<String>> context) {
//				return context.getSource().stream().map(role -> role.getName().getValue()).collect(Collectors.toSet());
//			}
//		};
		
		Converter<String, String> encodePassword = new Converter<String, String>() {

			public String convert(MappingContext<String, String> context) {
				return encoder.encode(context.getSource());
			}
		};
//		Converter<Set<String>, Set<Role>> convertStringToRole = new Converter<Set<String>, Set<Role>>() {
//			public Set<Role> convert(MappingContext<Set<String>, Set<Role>> context) {
//				return roleService.getRolesByString(context.getSource());
//			}
//		};

//		PropertyMap<User, UserDTO> userToUserDTOMap = new PropertyMap<User, UserDTO>() {
//			protected void configure() {
//				using(convertRoleToString).map(source.getRoles()).setRoles(null);
//			}
//		};
//		
		PropertyMap<TeacherDTO, Teacher> teacherDTOToTeacherMap = new PropertyMap<TeacherDTO, Teacher>() {
			protected void configure() {
				using(encodePassword).map(source.getPassword()).setPassword(null);
//				using(convertStringToRole).map(source.getRoles()).setRoles(null);
			}
		};
		
		PropertyMap<RegisterTeacherDTO, Teacher> registerTeacherDTOToTeacherMap = new PropertyMap<RegisterTeacherDTO, Teacher>() {
			protected void configure() {
				using(encodePassword).map(source.getPassword()).setPassword(null);
//				using(convertStringToRole).map(source.getRoles()).setRoles(null);
			}
		};
//		mapper.addMappings(userToUserDTOMap);
		mapper.addMappings(teacherDTOToTeacherMap);
		mapper.addMappings(registerTeacherDTOToTeacherMap);
	}


	public Teacher map(TeacherDTO teacherDTO) {
		Teacher mappedTeacher = mapper.map(teacherDTO, Teacher.class);
		return mappedTeacher;
	}
	
	public Teacher map(RegisterTeacherDTO registerTeacherDTO) {
		Teacher mappedTeacher = mapper.map(registerTeacherDTO, Teacher.class);
		return mappedTeacher;
	}

	public List<TeacherDTO> map(List<Teacher> teachers) {
		if (teachers == null || teachers.isEmpty()) {
			return Collections.emptyList();
		}

		return mapper.map(teachers, userDtoListType);
	}


	public TeacherDTO mapTeacher(Teacher teacher) {
		return mapper.map(teacher, TeacherDTO.class);
	}

	public TeacherProfileDTO mapTeacherProfile(Teacher teacher){
		return mapper.map(teacher, TeacherProfileDTO.class);
	}
}
