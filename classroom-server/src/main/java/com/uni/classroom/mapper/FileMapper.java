package com.uni.classroom.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.uni.classroom.dto.FileDTO;
import com.uni.classroom.model.File;

@Component
public class FileMapper {
	private static ModelMapper mapper = new ModelMapper();

	public FileDTO map(File dbFile) {
		FileDTO mappedFile = mapper.map(dbFile, FileDTO.class);
		return mappedFile;
	}

}
