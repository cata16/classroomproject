package com.uni.classroom.dto;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssignmentDTO {
	
	private Long id;
	@NotBlank
	private String title;
	@NotBlank
	private String description;
	
	private LocalDateTime dueDate;	
	
	private List<StudentAssignmentDTO> studentAssignments;
	
}
