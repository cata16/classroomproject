package com.uni.classroom.dto;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentProfileDTO {
	
	private Long id;
	@NotBlank
	private String email;

	private int yearOfStudy;
	
	private String phoneNumber;
	
	private String fullName;
	
	private String university;
	
	private String groupStudy;
	
}
