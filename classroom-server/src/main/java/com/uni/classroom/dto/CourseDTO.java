package com.uni.classroom.dto;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CourseDTO {

	private Long id;
	
	@NotBlank
	private String name;
	
	private Long code;
	
	private TeacherProfileDTO teacher;
//	private Set<StudentDTO> students;
//	private Long teacherId;
}
