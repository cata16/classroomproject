package com.uni.classroom.dto;

import java.time.LocalDateTime;

import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FileDTO {

	private Long id;
	private LocalDateTime date;
	private String extension;
	private String name;
//	private StudentAssignment studentAssignment;
}
