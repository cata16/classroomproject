package com.uni.classroom.dto;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeacherProfileDTO {
	
	private Long id;
	
	@NotBlank
	private String email;
	
	private String phoneNumber;
	
	private String fullName;
	
	private String department;
	
	private String university;
}
