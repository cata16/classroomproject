package com.uni.classroom.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterStudentDTO {
	
	@NotBlank
	private String email;
	
	@NotBlank
	private String password;
	private String matchingPassword;
	
	@NotBlank
	private String phoneNumber;
	
	@NotBlank
	private String fullName;
	
	@NotBlank
	private String university;
	
	@NotBlank
	private String groupStudy;
	
	@NotNull
	@Min(1)
	private int yearOfStudy;
}
