package com.uni.classroom.dto;

import java.util.List;

import com.uni.classroom.utils.AssignmentStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentAssignmentDTO {
	
	private Long id;
	
	private String feedback;
	
	private int grade;
	
	private AssignmentStatus status;
	
	private List<FileDTO> files;
	
	private String title;
	
	private String description;
	
	private StudentDTO student;
}
