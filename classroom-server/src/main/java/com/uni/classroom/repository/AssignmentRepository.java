package com.uni.classroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.uni.classroom.model.Assignment;

@Repository
public interface AssignmentRepository extends JpaRepository<Assignment, Long> {

	Assignment findByTitle(String name);

	List<Assignment> findByCourseId(Long id);
}
