package com.uni.classroom.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.uni.classroom.model.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {

	Teacher findByEmail(String email);

}
