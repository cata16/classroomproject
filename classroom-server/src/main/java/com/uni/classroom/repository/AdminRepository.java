package com.uni.classroom.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.uni.classroom.model.Admin;

public interface AdminRepository extends JpaRepository<Admin, Long> {


}