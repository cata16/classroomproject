package com.uni.classroom.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.uni.classroom.model.Course;
import com.uni.classroom.model.Student;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

	Course findByName(String name);

	Course findByCode(Long courseCode);

	List<Course> findByTeacherId(Long teacherId);

	List<Course> findByStudents_Id(Long studentId);
	
}
