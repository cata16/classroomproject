package com.uni.classroom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.uni.classroom.model.Course;
import com.uni.classroom.model.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {

	Student findByEmail(String email);

}
