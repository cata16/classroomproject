package com.uni.classroom.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.uni.classroom.model.Assignment;
import com.uni.classroom.model.Student;
import com.uni.classroom.model.StudentAssignment;
import com.uni.classroom.utils.AssignmentStatus;

public interface StudentAssignmentRepository extends JpaRepository<StudentAssignment, Long> {

	@Transactional
	@Modifying
	@Query("UPDATE StudentAssignment SET grade = :grade WHERE id = :studentAssignmentId")
	void updateGrade(int grade, Long studentAssignmentId) ;
	
	@Transactional
	@Modifying
	@Query("UPDATE StudentAssignment SET feedback = :feedback WHERE id = :studentAssignmentId")
	void updateFeedback(String feedback, Long studentAssignmentId) ;
	
	@Transactional
	@Modifying
	@Query("UPDATE StudentAssignment SET status_id = :statusId WHERE id = :studentAssignmentId")
	void updateStatus(AssignmentStatus status, Long studentAssignmentId) ;
	
//	StudentAssignment findByAssignmentAndStudent( Assignment assignment , Student student  );

	StudentAssignment findByAssignmentIdAndStudentId(Long assignmentId, Long studentId);
	
}
