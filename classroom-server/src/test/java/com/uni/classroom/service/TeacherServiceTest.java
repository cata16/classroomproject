package com.uni.Classroom.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.uni.classroom.model.Teacher;
import com.uni.classroom.repository.TeacherRepository;
import com.uni.classroom.service.TeacherService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TeacherServiceTest {

	@MockBean
	private TeacherRepository teacherRepository;

	@Autowired
	private TeacherService teacherService;

	@Test
	public void testSave() {
		Teacher teacherTest = new Teacher();
		teacherTest.setEmail("testTeacher@gmail.com");
		teacherTest.setDepartment("IT");
		teacherTest.setFullName("Ioan Luca");
		teacherTest.setPassword("1234");
		teacherTest.setPhoneNumber("0784184484");
		teacherTest.setUniversity("Universitatea din Craiova");

		Mockito.when(teacherRepository.save(teacherTest)).thenReturn(teacherTest);

		assertThat(teacherService.save(teacherTest)).isEqualTo(teacherTest);
	}

	@Test
	public void testFindById() {
		Teacher teacherTest = new Teacher();
		teacherTest.setEmail("test2Teacher@gmail.com");
		teacherTest.setDepartment("IT");
		teacherTest.setFullName("Ioan Luca Caragiale");
		teacherTest.setPassword("1234");
		teacherTest.setPhoneNumber("0784184454");
		teacherTest.setUniversity("Universitatea din Bucuresti");
		teacherTest.setId(Long.valueOf(50));
		
		Optional<Teacher> returnCacheValue = Optional.of((Teacher) teacherTest);
		Mockito.when(teacherRepository.findById(Long.valueOf(50))).thenReturn(returnCacheValue);

		assertThat(teacherService.findById(Long.valueOf(50))).isEqualTo(teacherTest);
	}
	
	@Test
	public void testFindByEmail() {
		Teacher teacherTest = new Teacher();
		teacherTest.setEmail("test3Teacher@gmail.com");
		teacherTest.setDepartment("IT");
		teacherTest.setFullName("Ioan Luca Caragiale");
		teacherTest.setPassword("1234");
		teacherTest.setPhoneNumber("0784184454");
		teacherTest.setUniversity("Universitatea din Bucuresti");

		Mockito.when(teacherRepository.findByEmail("test3Teacher@gmail.com")).thenReturn(teacherTest);

		assertThat(teacherService.findByEmail("test3Teacher@gmail.com")).isEqualTo(teacherTest);
	}
}
