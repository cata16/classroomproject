package com.uni.Classroom.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.uni.classroom.model.Course;
import com.uni.classroom.model.Student;
import com.uni.classroom.repository.StudentRepository;
import com.uni.classroom.service.StudentService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StudentServiceTest {

	@MockBean
	private StudentRepository studentRepository;

	@Autowired
	private StudentService studentService;

	@Test
	public void testSave() {
		Student studentTest = new Student();
		studentTest.setEmail("testStudent@gmail.com");
		studentTest.setFullName("Marin Ion");
		studentTest.setPassword("1234");
		studentTest.setPhoneNumber("0784184484");
		studentTest.setUniversity("Universitatea din Craiova");
		studentTest.setGroupStudy("3.1.A");
		studentTest.setYearOfStudy(3);

		Mockito.when(studentRepository.save(studentTest)).thenReturn(studentTest);

		assertThat(studentService.save(studentTest)).isEqualTo(studentTest);

	}

	@Test
	public void testFindById() {
		Student studentTest = new Student();
		studentTest.setEmail("test2Student@gmail.com");
		studentTest.setFullName("Marin Ion Luca");
		studentTest.setPassword("1234");
		studentTest.setPhoneNumber("0784134484");
		studentTest.setUniversity("Universitatea din Craiova");
		studentTest.setGroupStudy("3.1.B");
		studentTest.setYearOfStudy(3);
		studentTest.setId(Long.valueOf(100));

		Optional<Student> returnCacheValue = Optional.of((Student) studentTest);
		Mockito.when(studentRepository.findById(Long.valueOf(100))).thenReturn(returnCacheValue);

		assertThat(studentService.findById(Long.valueOf(100))).isEqualTo(studentTest);

	}

	@Test
	public void testFindByEmail() {
		Student studentTest = new Student();
		studentTest.setEmail("test3Student@gmail.com");
		studentTest.setFullName("Marin Ion Luca Vali");
		studentTest.setPassword("1234");
		studentTest.setPhoneNumber("0784134284");
		studentTest.setUniversity("Universitatea din Craiova");
		studentTest.setGroupStudy("3.1.C");
		studentTest.setYearOfStudy(3);

		Mockito.when(studentRepository.findByEmail("test3Student@gmail.com")).thenReturn(studentTest);

		assertThat(studentService.findByEmail("test3Student@gmail.com")).isEqualTo(studentTest);
	}
	
	@Test
	public void testGetCourses() {
		Course course1 = new Course();
		course1.setId(Long.valueOf(112));
		course1.setCode(Long.valueOf(112));
		course1.setName("course1");
		
		Course course2 = new Course();
		course2.setId(Long.valueOf(113));
		course2.setCode(Long.valueOf(113));
		course2.setName("course2");
		
		List<Course> listCourses = new ArrayList<Course>();
		listCourses.add(course1);
		listCourses.add(course2);
		
		Student studentTest = new Student();
		studentTest.setEmail("test4Student@gmail.com");
		studentTest.setFullName("Marin Marian");
		studentTest.setPassword("1234");
		studentTest.setPhoneNumber("0784384484");
		studentTest.setUniversity("Universitatea din Craiova");
		studentTest.setGroupStudy("3.1.D");
		studentTest.setYearOfStudy(3);
		
		studentTest.setCourses(listCourses);
		
		assertThat(studentService.getCourses(studentTest)).isEqualTo(listCourses);	
	}

}
