package com.uni.Classroom.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.uni.classroom.configuration.IConfiguration;
import com.uni.classroom.model.Assignment;
import com.uni.classroom.model.Course;
import com.uni.classroom.model.Student;
import com.uni.classroom.model.StudentAssignment;
import com.uni.classroom.repository.FileRepository;
import com.uni.classroom.service.FileService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FileServiceTest {
	
	@Value("${path.home.saving}")
	private String savingFilePath;

	@MockBean
	private FileRepository fileRepository;
	
	@MockBean
	private IConfiguration config;

	@Autowired
	private FileService fileService;

	@Test
	public void testGetFileById() {
		
		com.uni.classroom.model.File dbfile = new com.uni.classroom.model.File();
		dbfile.setId(Long.valueOf(1));
		dbfile.setExtension("pdf");
		dbfile.setName("file");
		
		//File diskFile = new File("C:\\Users\\Cata\\Desktop\\FilesDownloadedProject\\","1" + "." + "pdf");
		File diskFile = new File(savingFilePath,"1.pdf");
		Optional<com.uni.classroom.model.File> returnCacheValue = Optional.of((com.uni.classroom.model.File) dbfile);
		Mockito.when(fileRepository.findById(Long.valueOf(1))).thenReturn(returnCacheValue);
		Mockito.when(config.getFileSavePath()).thenReturn("");
		assertThat(fileService.getFileById(Long.valueOf(1))).isEqualTo(diskFile);

	}
	
	@Test
	public void testFindById() {
		com.uni.classroom.model.File dbfile = new com.uni.classroom.model.File();
		dbfile.setId(Long.valueOf(1));
		dbfile.setExtension("pdf");
		dbfile.setName("file");

		Optional<com.uni.classroom.model.File> returnCacheValue = Optional.of((com.uni.classroom.model.File) dbfile);
		Mockito.when(fileRepository.findById(Long.valueOf(1))).thenReturn(returnCacheValue);

		assertThat(fileService.findById(Long.valueOf(1))).isEqualTo(dbfile);

	}
	
	@Test
	public void testDeleteById() {
		com.uni.classroom.model.File dbfile = new com.uni.classroom.model.File();
		dbfile.setId(Long.valueOf(1));
		dbfile.setExtension("pdf");
		dbfile.setName("file");
		
		Optional<com.uni.classroom.model.File> returnCacheValue = Optional.of((com.uni.classroom.model.File) dbfile);
		Mockito.when(fileRepository.findById(Long.valueOf(1))).thenReturn(returnCacheValue);
		assertThat(fileService.deleteById(Long.valueOf(1))).isEqualTo(dbfile);

	}
	
	/*
	@Test
	public void testUpload() {
		com.uni.classroom.model.File dbfile = new com.uni.classroom.model.File();
		dbfile.setId(Long.valueOf(1));
		dbfile.setExtension("pdf");
		dbfile.setName("file");
		
		Student student = new Student();
		student.setId(Long.valueOf(1));
		student.setEmail("testStudent@gmail.com");
		student.setFullName("Marin Ion");
		student.setPassword("1234");
		student.setPhoneNumber("0784184484");
		student.setUniversity("Universitatea din Craiova");
		student.setGroupStudy("3.1.A");
		student.setYearOfStudy(3);
		
		Assignment assignment = new Assignment();
		assignment.setId(Long.valueOf(1));
		assignment.setTitle("Homework1");
				
		StudentAssignment studentAssignment = new StudentAssignment();
		studentAssignment.setId(Long.valueOf(1));
		studentAssignment.setStudent(student);
		studentAssignment.setAssignment(assignment);
		List<StudentAssignment> studentAssignments = new ArrayList<StudentAssignment>();
		studentAssignments.add(studentAssignment);
		assignment.setStudentAssignments(studentAssignments);
		
		InputStream fileStream = null;
		
		Mockito.when(fileRepository.save(dbfile)).thenReturn(dbfile);

		try {
			assertThat(fileService.upload("1", fileStream, Long.valueOf(1), Long.valueOf(1))).isEqualTo(dbfile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}*/
	
	
}
