package com.uni.Classroom.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.uni.classroom.model.Course;
import com.uni.classroom.model.Student;
import com.uni.classroom.repository.CourseRepository;
import com.uni.classroom.service.CourseService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CourseServiceTest {

	@MockBean
	private CourseRepository courseRepository;

	@Autowired
	private CourseService courseService;

	@Test
	public void testLeaveStudent() {
		Course course = new Course();
		course.setId(Long.valueOf(1));
		course.setCode(Long.valueOf(1));
		course.setName("Informatics");
		
		Student student = new Student();
		student.setEmail("testStudent@gmail.com");
		student.setFullName("Marin Ion Luca");
		student.setPassword("1234");
		student.setPhoneNumber("0784134484");
		student.setUniversity("Universitatea din Craiova");
		student.setGroupStudy("3.1.B");
		student.setYearOfStudy(3);
		student.setId(Long.valueOf(1));
		
		List<Student> students = new ArrayList<Student>();
		students.add(student);
		course.setStudents(students);
		
		assertTrue("Student leave the course", courseService.leaveStudent(course, student.getId()));
	}
	
	@Test
	public void testAssignStudent() {
		Course course = new Course();
		course.setId(Long.valueOf(1));
		course.setCode(Long.valueOf(1));
		course.setName("Informatics");
		
		Student student = new Student();
		student.setEmail("testStudent@gmail.com");
		student.setFullName("Marin Ion Luca");
		student.setPassword("1234");
		student.setPhoneNumber("0784134484");
		student.setUniversity("Universitatea din Craiova");
		student.setGroupStudy("3.1.B");
		student.setYearOfStudy(3);
		student.setId(Long.valueOf(1));
		
		assertTrue("Student was assigned to the course", courseService.assignStudent(course, student.getId()));
	}
	
	@Test
	public void testJoinStudent() {
		Course course = new Course();
		course.setId(Long.valueOf(1));
		course.setCode(Long.valueOf(1));
		course.setName("Informatics");
		
		Student student = new Student();
		student.setEmail("testStudent@gmail.com");
		student.setFullName("Marin Ion Luca");
		student.setPassword("1234");
		student.setPhoneNumber("0784134484");
		student.setUniversity("Universitatea din Craiova");
		student.setGroupStudy("3.1.B");
		student.setYearOfStudy(3);
		student.setId(Long.valueOf(1));
		
		assertTrue("Student join the course", courseService.joinStudent(course, student.getId()));
	}
	
	@Test
	public void testSave() {
		Course course = new Course();
		course.setId(Long.valueOf(1));
		course.setCode(Long.valueOf(1));
		course.setName("Informatics");
		
		Mockito.when(courseRepository.save(course)).thenReturn(course);

		assertThat(courseService.save(course)).isEqualTo(course);

	}

	@Test
	public void testFindById() {
		Course course = new Course();
		course.setId(Long.valueOf(1));
		course.setCode(Long.valueOf(1));
		course.setName("Informatics");

		Optional<Course> returnCacheValue = Optional.of((Course) course);
		Mockito.when(courseRepository.findById(Long.valueOf(1))).thenReturn(returnCacheValue);

		assertThat(courseService.findById(Long.valueOf(1))).isEqualTo(course);

	}

	@Test
	public void testFindByName() {
		Course course = new Course();
		course.setId(Long.valueOf(1));
		course.setCode(Long.valueOf(1));
		course.setName("Informatics");

		Mockito.when(courseRepository.findByName("Informatics")).thenReturn(course);

		assertThat(courseService.findByName("Informatics")).isEqualTo(course);
	}
	
	@Test
	public void testFindByCode() {
		Course course = new Course();
		course.setId(Long.valueOf(1));
		course.setCode(Long.valueOf(1));
		course.setName("Informatics");
		
		Mockito.when(courseRepository.findByCode((long)1)).thenReturn(course);

		assertThat(courseService.findByCode((long)1)).isEqualTo(course);
	}
	
	@Test
	public void testFindByStudentId() {
		Course course = new Course();
		course.setId(Long.valueOf(1));
		course.setCode(Long.valueOf(1));
		course.setName("Informatics");
		
		Student student = new Student();
		student.setEmail("testStudent@gmail.com");
		student.setFullName("Marin Ion Luca");
		student.setPassword("1234");
		student.setPhoneNumber("0784134484");
		student.setUniversity("Universitatea din Craiova");
		student.setGroupStudy("3.1.B");
		student.setYearOfStudy(3);
		student.setId(Long.valueOf(1));
		
		course.joinStudent(student);
		List<Course> courses = new ArrayList<Course>();
		courses.add(course);
		
		Mockito.when(courseRepository.findByStudents_Id((long)1)).thenReturn(courses);

		assertThat(courseService.findByStudentId((long)1)).isEqualTo(courses);
	}
	
	/*
	@Test
	public void testGetAllStudents() {
		Student student1 = new Student();
		student1.setEmail("test1Student@gmail.com");
		student1.setFullName("Marin Ion Luca");
		student1.setPassword("1234");
		student1.setPhoneNumber("0784134484");
		student1.setUniversity("Universitatea din Craiova");
		student1.setGroupStudy("3.1.B");
		student1.setYearOfStudy(3);
		student1.setId(Long.valueOf(1));
		
		Student student2 = new Student();
		student2.setEmail("test2Student@gmail.com");
		student2.setFullName("Popescu Mihai");
		student2.setPassword("1234");
		student2.setPhoneNumber("0784134486");
		student2.setUniversity("Universitatea din Craiova");
		student2.setGroupStudy("3.1.B");
		student2.setYearOfStudy(3);
		student2.setId(Long.valueOf(2));
		
		List<Student> listStudents = new ArrayList<Student>();
		listStudents.add(student1);
		listStudents.add(student2);
		
		Course course = new Course();
		course.setId(Long.valueOf(3));
		course.setCode(Long.valueOf(3));
		course.setName("Informatics");
		
		course.setStudents(listStudents);
		
		assertThat(courseService.getAllStudents(course.getId())).isEqualTo(listStudents);	
	}*/
}
